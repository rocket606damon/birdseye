<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package birdseye
 */

?>

</main><!-- #content -->

<?php
	if(!is_singular('product')) {
		$veg = 'veg';
	} else {
		$veg = '';
	}
?>
<footer class="site-footer <?php echo $veg; ?>">
	<div class="bg"></div>
	<div class="footer-msg flex-row bg-gradient">
		<div class="container-site">
			<?php
				$posts = get_field('footer_msg_rel');
				if($posts) :
			?>
				<?php foreach ($posts as $post) : setup_postdata($post); ?>
					<h3 class="headline-f white"><?php the_field('footer_message'); ?></h3>
				<?php endforeach; wp_reset_postdata(); ?>
			<?php else : ?>	
				<h3 class="headline-f white">Our veggies contain no artificial<br>colors or artificial flavors. They<br>are simply fresh vegetables,<br>flash frozen!</h3>
			<?php endif; ?>
		</div>
	</div>
	<div class="footer-nav container-site">
		<?php 
			wp_nav_menu( 
				array( 
					'theme_location' => 'secondary',
					'menu_id' => 'footer-nav',
					'menu_class' => 'flex-row footer-nav-container',
					'container' => ''
				) 
			); 

			include('components/social.php');
		?>	
	</div>
</footer>

<?php 
	include('assets/icons/svg-defs.svg');
	wp_footer(); 
?>
<?php if(is_page_template('page-template-our-roots.php')) : ?>
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$('#snow').snowfall({image:'/wp-content/themes/birdseye/assets/images/our-roots/flake.png', minSize:4, maxSize:8});
		});
	</script>
<?php endif; ?>

<?php if(is_singular('recipe')) : ?>
	<script type="text/javascript">
		(function(d){
		    var f = d.getElementsByTagName('SCRIPT')[0], p = d.createElement('SCRIPT');
		    p.type = 'text/javascript';
		    p.async = true;
		    p.src = '//assets.pinterest.com/js/pinit.js';
		    f.parentNode.insertBefore(p, f);
		}(document));
	</script>
<?php endif; ?>
</body>
</html>
