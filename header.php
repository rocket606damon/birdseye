<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package birdseye
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
<link rel="manifest" href="/manifest.json">
<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#04b7e3">
<meta name="theme-color" content="#ffffff">
<meta name="google-site-verification" content="eNvHM8P1TBZW-hhMYsqBNaJWI_4A743jVtwr6SHe8ns" />

<!-- Typekit Fonts -->
<script>
  (function(d) {
    var config = {
      kitId: 'cxd1nxj',
      scriptTimeout: 3000,
      async: true
    },
    h=d.documentElement,t=setTimeout(function(){h.className=h.className.replace(/\bwf-loading\b/g,"")+" wf-inactive";},config.scriptTimeout),tk=d.createElement("script"),f=false,s=d.getElementsByTagName("script")[0],a;h.className+=" wf-loading";tk.src='https://use.typekit.net/'+config.kitId+'.js';tk.async=true;tk.onload=tk.onreadystatechange=function(){a=this.readyState;if(f||a&&a!="complete"&&a!="loaded")return;f=true;clearTimeout(t);try{Typekit.load(config)}catch(e){}};s.parentNode.insertBefore(tk,s)
  })(document);
</script>

<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-11583799-16', 'auto');
  ga('send', 'pageview');
</script>

<?php wp_head(); ?>
<script><?php include('assets/js/global/modernizr-custom.js'); ?></script>
<?php //kd_add_css_to_page(); ?>
<?php if(is_singular('product') || is_singular('recipe')) : ?>
	<meta name="ps-key" content="1793-586ec8219a78ea8b239f183b">
<!-- 	<script src="//cdn.pricespider.com/1/lib/ps-widget.js" async></script> -->
<?php endif; ?>
<?php if(is_page_template('page-template-wtb.php')) : ?>
	<meta name="ps-key" content="1793-58adad1be3f5ea6b0bffd8b3">
	<script src="//cdn.pricespider.com/1/lib/ps-widget.js" async></script>
<?php endif; ?>
<?php if(is_singular('product') || is_singular('recipe')) : ?>
	<meta name="ps-key" content="1793-586ec8219a78ea8b239f183b">
	<script src="//cdn.pricespider.com/1/lib/ps-widget.js" async></script>
<?php endif; ?>
</head>

<body <?php body_class(); ?>>
	<?php
		$header_alt = array(
			'page-template-home.php',
			'page-template-our-farms.php',
			'page-template-our-roots.php',
			'page-template-products-listing.php',
			'page-template-mission.php'
		);
		if(is_page_template($header_alt) || is_singular('product')) {
			$header_style = 'header-alt';
		} else {
			$header_style = '';
		}
	?>

	<header class="site-header <?php echo $header_style; ?>">
		<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'birdseye' ); ?></a>
		<a href="/" class="site-logo">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/images/global/birdseye-logo.svg">
		</a>
		<button class="nav-toggle">
			<span class="clip">Navigation</span>
			<span class="burger top"></span>
			<span class="burger middle"></span>
			<span class="burger bottom"></span>
		</button>
		<nav class="site-nav">
		<!--data-top="top: 0%; margin-top: 0px;"
			data-bottom-top="top: 100%; margin-top: -5em;"
			data-anchor-target=".panel-mission"-->
			<div class="main-nav-container">
				<?php 
					wp_nav_menu( 
						array( 
							'theme_location' => 'primary',
							'menu_id' => 'main-nav',
							'menu_class' => 'main-nav',
							'container' => ''
						) 
					); 
				?>	
			</div>
		</nav>
		<?php include('components/social.php'); ?>
	</header>

	<main id="content" class="site-content">