<?php
/* Template Name: Where To Buy */

//kd_enqueue_stylesheet('recipes');
get_header(); ?>

<div id="nonce" data-security="<?php echo wp_create_nonce( 'security_check' ); ?>"></div>

<section class="general-content container-site">
	<header class="post-header">
		<h1 class="headline-l"><?php the_title(); ?></h1>
	</header>
	<?php 
		while(have_posts()) : the_post();
			the_content();
		endwhile;
	?>
	<div class="wtb-checkbox">
		<div class="checkbox">
			<input id="wtb-steamfresh" type="checkbox" name="steamfresh" value="BIRDS EYE STEAMFRESH" checked>
			<label for="wtb-steamfresh">Birds Eye Steamfresh</label>
		</div>
		<div class="checkbox">
			<input id="wtb-be" type="checkbox" name="be" value="BIRDS EYE">
			<label for="wtb-be">Birds Eye</label>
		</div>
		<div class="checkbox">
			<input id="wtb-voila" type="checkbox" name="voila" value="BIRDS EYE VOILA!">
			<label for="wtb-voila">Birds Eye Voila!</label>
		</div>
		<div class="checkbox">
			<input id="wtb-signatureskil" type="checkbox" name="signatureskil" value="BIRDS EYE SIGNATURE SKILLETS">
			<label for="wtb-signatureskil">Birds Eye Signature Skillets</label>
		</div>
		<div class="checkbox">
			<input id="wtb-cw" type="checkbox" name="cw" value="C&W">
			<label for="wtb-cw">C&W</label>
		</div>
	</div>
	<div class="wtb-search">
		<input id="wtbFilter" class="ps-search-input type="search" name="where to buy" placeholder="Search Birds Eye Products" />
	</div>
	<div class="container-ps-catalog"></div>
</section>

<script id="psCatResults" type="text/x-handlebars-template">
	{{#each pscats}}
		<div class="ps-widget {{val}}" ps-tag="{{{val}}}">
			<h3 class="headline-s">{{{name}}}</h3>
		</div>
	{{/each}}
</script>

<?php
	get_footer();
