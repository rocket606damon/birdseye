<?php
/* Template Name: Mission Pg */
//kd_enqueue_stylesheet('mission');
get_header(); ?>

<section class="hero-mission hero-video">
	<video id="hero-video" class="fullscreen-video" muted preload="auto">
		<source data-video="<?php echo get_stylesheet_directory_uri(); ?>/assets/videos/Birds_Eye_Our_Story_Video-v4.mp4" type="video/mp4">
	</video>	
	<div class="container-mission">
		<div class="hero-msg">
			<h1 class="headline-block lead-64 white">
				<span class="cap" style="font-size:56.25%;margin-top:-0.5em;">Our Mission</span>
				<span class="cap lines lines-mission" style="font-size:28.125%;">Is</span>
				<span>Simple</span>
			</h1>	
		</div>	
	</div>
	<div class="append-l" data-set="mission-copy"></div>
</section>
<div class="append-s" data-set="mission-copy">
	<div class="hero-mission-copy container-mission">
		<p>Some companies have lofty goals. To fight disease. Or feed the world. Or save the planet. At Birds Eye our ambition is humble. We just want to help people eat more vegetables.</p>
		<p>So, every day, for nearly a century, we’ve worked tirelessly to make  vegetables irresistible. Easier to buy. Store. Prepare. And serve. More delicious. More delightful. Because when people get more of their daily nutrition from vegetables and plants, remarkable things happen.</p>
		<p>They experience less obesity, diabetes, heart disease and cancer. Farmers can feed more people using less land, water, and energy. Fewer chemicals are needed. Fewer greenhouse gases are released. Which means that helping people eat more vegetables may actually help fight disease, feed the world and save the planet.</p>
		<p>It seems your Mom was right all along: it’s good to eat vegetables. So we make vegetables good to eat.</p>
	</div>
</div>

<section class="flex-row img-blocks mission-blocks">
	<div class="img-block flex-col roots-block" title="Meet Clarence Birdseye">
		<div class="block-content">
			<h2 class="headline-s white">Our Roots</h2>
			<p>Meet Clarence Birdseye, a visionary who changed the way <br>America eats vegetables. He invented frozen foods as we <br>know them today. The rest, as they say, is Birds Eye history.</p>
			<a href="<?php echo home_url(); ?>/our-story/our-roots" class="btn btn-l">See How We Started</a>	
		</div>
	</div>
	<div class="img-block flex-col farm-block">
		<div class="block-content">
			<h2 class="headline-s white">Fresh From the Farm</h2>
			<p>Birds Eye has a long history of working with family <br>farmers. Our veggies ripen in the field—not on the <br>truck. See where our vegetables are grown.</p>
			<a href="<?php echo home_url(); ?>/our-story/our-farms" class="btn btn-l">See Where Goodness Grows</a>	
		</div>
	</div>
</section>
<?php include('components/panels/products-all.php'); ?>
<?php include('components/panels/media.php'); ?>



<?php
	get_footer();