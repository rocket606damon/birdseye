<ul class="social-links">
	<?php if(is_page_template('page-template-home.php')) : ?>
		<li class="buy-link-alt"><a href="<?php echo home_url(); ?>/where-to-buy">Where To Buy</a></li>
	<?php endif; ?>
	<li>
		<a href="https://www.facebook.com/BirdsEyeVegetables" target="_blank">
			<svg class="fb">
				<use xlink:href="#icon-facebook"></use>
			</svg>
			<span class="clip">facebook</span>
		</a>
	</li>
	<li>
		<a href="https://twitter.com/birdseye" target="_blank">
			<svg class="twitter">
				<use xlink:href="#icon-twitter"></use>
			</svg>
			<span class="clip">twitter</span>
		</a>
	</li>
	<li>
		<a href="https://www.pinterest.com/realbirdseye/" target="_blank">
			<svg class="pin">
				<use xlink:href="#icon-pinterest"></use>
			</svg>
			<span class="clip">pinterest</span>
		</a>
	</li>
</ul>