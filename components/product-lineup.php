<?php if(have_rows('featured_product_lineup')) : ?>
	<?php $i = 1; ?>
	<?php while(have_rows('featured_product_lineup')) : the_row(); ?>
		<?php 
			$rows = get_field('featured_product_lineup');
			$row_count = count($rows);
			if($row_count === 1) {
				$pos = 'first';
			} else {
				$pos = '';
			}
		?>

		<section class="product-lineup container-site new-lineup group-<?php echo $i; ?>">
			<header>
				<h3 class="headline-s"><?php the_sub_field('product_lineup_headline'); ?></h3>
				<?php if(get_sub_field('product_lineup_copy')) : ?>
					<p class="prod-feature-title light"><?php the_sub_field('product_lineup_copy'); ?></p>
				<?php endif; ?>
			</header>

			<?php 
				$posts = get_sub_field('product_selection');
				$post_count = count($posts);
				if($posts) : 
			?>
				<div class="flex-row prod-feature">
					<?php foreach($posts as $post) : setup_postdata($post); ?>	
						<a href="<?php the_permalink(); ?>" class="featured flex-col">
							<div class="img-wrap">
								<?php 
									$prod_img = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
								?>
								<img src="<?php echo $prod_img[0]; ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>">
							</div>
							<div class="copy-wrap">
								<h3 class="prod-feature-title"><?php the_title(); ?></h3>
							</div>
						</a>
					<?php endforeach; wp_reset_postdata(); ?>
				</div>
			<?php endif; ?>
			<?php if($post_count > 4) : ?>
				<div class="lineup-footer">
					<p class="product-toggle" data-all="See All" data-less="See Less">See All</p>
					<span class="down">
						<svg>
							<use xlink:href="#icon-down"></use>
						</svg>
					</span>	
				</div>
			<?php endif; ?>
		</section>
		<?php $i++; ?>
	<?php endwhile; ?>
<?php endif; ?>