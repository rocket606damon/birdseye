
	<section class="panel panel-products prod-details">
		<div class="container-site flex-row">

			<div class="product-image">
				<a href="<?php the_permalink(); ?>">
					<img src="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' )[0]; ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>">
				</a>
			</div>
			<div class="product-info">
				<h1 class="headline-s"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
				
				<div class="the-content"><?php the_content(); ?></div>
				<hr>
				<div class="available-in">Available in: <span><?php the_field('size'); ?> <?php the_field('container'); ?></span></div>
				<hr>
				<div class="nutrition-facts">
					<span>Nutrition Facts</span>
					<div class="nutrition-label">
						<p>Servings per Container <?php the_field('serving_per_container'); ?></p>
						<p>Servings Size <?php the_field('serving_size'); ?></p>
						<br>
						<p>Amount per Serving</p>
						<?php if(is_single(array(4107,4109,4121,4129,4147,4131,4187,4153,4157,4189,4165,4191,4275,4273,4277,4279))): ?>
						<p><strong>Calories <?php the_field('num_calories'); ?></strong></p>	
						<?php else: ?>	
						<p><strong>Calories <?php the_field('num_calories'); ?></strong> | Calories From Fat <?php the_field('num_calories_from_fat'); ?></p>
						<?php endif; ?>
						<br>
						<p>% Daily Value *</p>
						<p class="dot-wrapper">
							<span><strong>Total Fat</strong> <?php the_field('num_total_fat'); ?>g</span>
							<span><?php the_field('fat'); ?>%</span>
						</p>
						<p class="dot-wrapper">
							<span>Saturated Fat <?php the_field('num_saturated_fat'); ?>g</span>
							<span><?php the_field('saturated_fat'); ?>%</span>
						</p>
						<p>Trans Fat <?php the_field('num_trans_fat'); ?>g</p>
						<p class="dot-wrapper">
							<span><strong>Cholesterol</strong> <?php the_field('num_cholesterol'); ?>mg</span>
							<span><?php the_field('cholesterol'); ?>%</span>
						</p>
						<p class="dot-wrapper">
							<span><strong>Sodium</strong> <?php the_field('num_sodium'); ?>mg</span>
							<span><?php the_field('sodium'); ?>%</span>
						</p>
						<p class="dot-wrapper">
							<span><strong>Total Carbohydrates</strong> <?php the_field('num_total_carbs'); ?>g</span>
							<span><?php the_field('total_carbs'); ?>%</span>
						</p>
						<p class="dot-wrapper">
							<span>Dietary Fiber <?php the_field('num_fiber'); ?>g</span>
							<span><?php the_field('fiber'); ?>%</span>
						</p>
						<p>Total Sugars <?php the_field('num_sugar'); ?>g</p>
						<p><strong>Protein</strong> <?php the_field('num_protein'); ?>g</p>
						<p class="dot-wrapper">
							<span>Vitamin A</span>
							<span><?php the_field('vitamin_a'); ?>%</span>
						</p>
						<p class="dot-wrapper">
							<span>Vitamin C</span>
							<span><?php the_field('vitamin_c'); ?>%</span>
						</p>
						<p class="dot-wrapper">
							<span>Calcium</span>
							<span><?php the_field('calcium'); ?>%</span>
						</p>
						<p class="dot-wrapper">
							<span>Iron</span>
							<span><?php the_field('iron'); ?>%</span>
						</p>
					</div>
				</div>

				<hr>

				<?php 
					$related_recipes = get_field('recipe_panel');
					if($related_recipes) {
						$id = '#relatedRecipes';
					} 
				?>
				<div class="btns flex-row">
					<?php
						if(get_field('upc_12')) {
							$upc = get_field('upc_12');
						} elseif(get_field('upc')) {
							$upc = get_field('upc');
						} elseif(get_field('upc_14')) {
							$upc = get_field('upc_14');
						}
					?>
					<a href="" class="btn btn-s ps-widget" ps-sku="<?php echo $upc; ?>"><span class="placeholder">Where to buy</span></a>
					<?php if(is_singular('product')) : ?>
						<a href="#relatedRecipes" class="btn btn-m">Recipe suggestions</a>		
					<?php endif; ?>
				</div>
			</div>

		</div>
	</section>
