<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<?php

	$terms = get_the_terms($post->ID, 'product_category');
	$parents = array();
	$ids = array();
	for ($a = 0; $a < count($terms); $a++) {
		$parents[] = $terms[$a]->parent;
		$ids[] = $terms[$a]->term_taxonomy_id;
	}

	$lowest = array_values(array_diff($ids, $parents))[0];

	$subcategories = get_child_cats($lowest, '');
	$exclude = array();
	for ($b = 0; $b < count($subcategories); $b++) {
		$exclude[] = $subcategories[$b]->slug;
	}

	$query = new WP_Query( array(
		'post_type' => 'product',
		'post__not_in' => array($post->ID),
		'posts_per_page' => -1,
		'tax_query' => array(
			array(
				'taxonomy' => 'product_category',
				'field' => 'term_id',
				'terms' => $lowest
			),
			array(
				'taxonomy' => 'product_category',
				'terms' => $exclude,
				'field' => 'slug',
				'operator' => 'NOT IN',
			))
	));

	if ($query->have_posts()) : ?>

		<section class="panel panel-varieties">
			<header class="varieties-header">
				<h2 class="headline-s">Other Varieties</h2>
			</header>
			<div class="container-site">
			<?php 
				$count = $query->post_count; 
				if($count < 4) {
					$slide_count = 'low';
				} else {
					$slide_count = '';
				}
			?>

				<div class="slider <?php echo $slide_count; ?>">

<?php
				while ($query->have_posts()) : $query->the_post();

					echo 	'<div class="product">' .
								'<a href="' . get_the_permalink() . '" class="flex-col" data-term="'.$lowest.'">' .
								'<div class="img-wrap"><img src="' . wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' )[0] . '" alt="'. get_the_title() .'" title="'. get_the_title() .'"></div>' .
								'<div class="copy-wrap flex-col"><p class="prod-feature-title">' . get_the_title() . '</p></div>' .
								'</a>' .
							'</div>';

				endwhile;

				wp_reset_postdata(); 

?>

				</div>
			</div>
		</section>

<?php endif; ?>

<?php endwhile; endif; ?>