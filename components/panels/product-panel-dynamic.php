<?php
	$posts = get_field('product_panel_selection');

	if($posts) :
?>

	<?php	
		foreach($posts as $post) : setup_postdata($post);

			$panel_copy = get_field('product_panel_copy');
			$copy_count = strlen($panel_copy);
			$img_full = get_field('full_panel_image');
			$img_half = get_field('panel_image');
			$text_module = get_field('text_module_position');
			$color = get_field('text_color');
			$overlay = get_field('overlay');

			if(get_field('product_panel_bg') == 'white') {
				$bg = 'white-bg';
			} else {
				$bg = 'full-img-bg';
			}

			if($copy_count <= '18') {
				$size = '';
			} else if(($copy_count >= '19') && ($copy_count <= '24')) {
				$size = 'btn-m';
			} else {
				$size = 'btn-l';
			}

			if(get_field('text_alignment') == 'right') {
				$text_alignment = 'panel-r';
			} else {
				$text_alignment = '';
			}

			if(get_field('text_module_position') == 'right') {
				$module_pos = 'txt-mod-r';
			} else {
				$module_pos = 'txt-mod-l';
			}

			if($color) {
				$text_color = 'white';
			} else {
				$text_color = '';
			}

			if($overlay) {
				$use_overlay = 'overlay';
			} else {
				$use_overlay = '';
			}

			$img_full_s = wp_get_attachment_image_src( $img_full, 'full-panel-s');
			$img_full_m = wp_get_attachment_image_src( $img_full, 'full-panel-m');
			$img_full_l = wp_get_attachment_image_src( $img_full, 'full-panel-l');

			$img_half_s = wp_get_attachment_image_src( $img_half, 'half-panel-s');
			$img_half_l = wp_get_attachment_image_src( $img_half, 'half-panel-l');
	?>


	<section class="panel panel-dynamic flex-col <?php echo $bg; ?> <?php echo $text_alignment; ?> <?php echo $use_overlay; ?>">
		<div class="container-xl flex-row <?php echo $module_pos; ?>">
			<div class="panel-msg">
				<h2 class="headline-s <?php echo $text_color; ?>"><?php the_field('product_panel_copy'); ?></h2>
				<a href="<?php the_field('product_panel_cta_link'); ?>" class="btn <?php echo $size; ?>"><?php the_field('product_panel_cta_button'); ?></a>
			</div>	
		
		<?php if($img_half) : ?>
			<div class="animate-box">
				<picture>
					<!--[if IE 9]><video style="display: none;"><![endif]-->
					<source media="(min-width: 87.5em)" srcset="<?php echo $img_half_l[0]; ?>" />
					<!--[if IE 9]></video><![endif]-->
					<img src="<?php echo $img_half_s[0]; ?>" alt="<?php the_title(); ?>" />
				</picture>
			</div>
			<?php elseif($img_full) : ?>
				<picture>
					<!--[if IE 9]><video style="display: none;"><![endif]-->
					<source media="(min-width: 87.5em)" srcset="<?php echo $img_full_l[0]; ?>" />
					<source media="(min-width: 50em)" srcset="<?php echo $img_full_m[0]; ?>" />
					<!--[if IE 9]></video><![endif]-->
					<img src="<?php echo $img_full_s[0]; ?>" alt="<?php the_title(); ?>" />
				</picture>
			<?php endif; ?>
		</div>
	</section>

<?php endforeach; wp_reset_postdata(); ?>

<?php else : ?>
	<?php 
		$panels = new WP_Query(
			array(
				'post_type' => 'product_panels',
				'posts_per_page' => 1,
				'post_status' => 'publish',
				'orderby' => 'rand',
				'tax_query' => array(
					'relation' => 'AND',
					array(
						'taxonomy' => 'panel_cta',
						'field' => 'slug',
						'terms' => 'more-frozen-facts'
					),
				),
			)
		);
		while($panels->have_posts()) : $panels->the_post();  

		$panel_copy = get_field('product_panel_copy');
		$copy_count = strlen($panel_copy);
		$img_full = get_field('full_panel_image');
		$img_half = get_field('panel_image');
		$text_module = get_field('text_module_position');
		$color = get_field('text_color');
		$overlay = get_field('overlay');

		if(get_field('product_panel_bg') == 'white') {
			$bg = 'white-bg';
		} else {
			$bg = 'full-img-bg';
		}

		if($copy_count <= '18') {
			$size = '';
		} else if(($copy_count >= '19') && ($copy_count <= '24')) {
			$size = 'btn-m';
		} else {
			$size = 'btn-l';
		}

		if(get_field('text_alignment') == 'right') {
			$text_alignment = 'panel-r';
		} else {
			$text_alignment = '';
		}

		if(get_field('text_module_position') == 'right') {
			$module_pos = 'txt-mod-r';
		} else {
			$module_pos = 'txt-mod-l';
		}

		if($color) {
			$text_color = 'white';
		} else {
			$text_color = '';
		}

		if($overlay) {
			$use_overlay = 'overlay';
		} else {
			$use_overlay = '';
		}

		$img_full_s = wp_get_attachment_image_src( $img_full, 'full-panel-s');
		$img_full_m = wp_get_attachment_image_src( $img_full, 'full-panel-m');
		$img_full_l = wp_get_attachment_image_src( $img_full, 'full-panel-l');

		$img_half_s = wp_get_attachment_image_src( $img_half, 'half-panel-s');
		$img_half_l = wp_get_attachment_image_src( $img_half, 'half-panel-l');
	?>
		<section class="panel panel-dynamic flex-col <?php echo $bg; ?> <?php echo $text_alignment; ?> <?php echo $use_overlay; ?>">
			<div class="container-xl flex-row <?php echo $module_pos; ?>">
				<div class="panel-msg">
					<h2 class="headline-s <?php echo $text_color; ?>"><?php the_field('product_panel_copy'); ?></h2>
					<a href="<?php the_field('product_panel_cta_link'); ?>" class="btn <?php echo $size; ?>"><?php the_field('product_panel_cta_button'); ?></a>
				</div>	
			
				<?php if($img_half) : ?>
					<div class="animate-box <?php echo $module_pos; ?> ">
						<picture>
							<!--[if IE 9]><video style="display: none;"><![endif]-->
							<source media="(min-width: 87.5em)" srcset="<?php echo $img_half_l[0]; ?>" />
							<!--[if IE 9]></video><![endif]-->
							<img src="<?php echo $img_half_s[0]; ?>" alt="<?php the_title(); ?>" />
						</picture>
					</div>
				<?php elseif($img_full) : ?>
					<picture>
						<!--[if IE 9]><video style="display: none;"><![endif]-->
						<source media="(min-width: 87.5em)" srcset="<?php echo $img_full_l[0]; ?>" />
						<source media="(min-width: 50em)" srcset="<?php echo $img_full_m[0]; ?>" />
						<!--[if IE 9]></video><![endif]-->
						<img src="<?php echo $img_full_s[0]; ?>" alt="<?php the_title(); ?>" />
					</picture>
				<?php endif; ?>
			</div>
		</section>
	<?php endwhile; wp_reset_postdata(); ?>
	<?php 
		$panels = new WP_Query(
			array(
				'post_type' => 'product_panels',
				'posts_per_page' => 1,
				'post_status' => 'publish',
				'orderby' => 'rand',
				'tax_query' => array(
					'relation' => 'AND',
					array(
						'taxonomy' => 'panel_cta',
						'field' => 'slug',
						'terms' => 'where-goodness-grows'
					),
				),
			)
		);
		while($panels->have_posts()) : $panels->the_post();  

		$panel_copy = get_field('product_panel_copy');
		$copy_count = strlen($panel_copy);
		$img_full = get_field('full_panel_image');
		$img_half = get_field('panel_image');
		$text_module = get_field('text_module_position');
		$color = get_field('text_color');
		$overlay = get_field('overlay');

		if(get_field('product_panel_bg') == 'white') {
			$bg = 'white-bg';
		} else {
			$bg = 'full-img-bg';
		}

		if($copy_count <= '18') {
			$size = '';
		} else if(($copy_count >= '19') && ($copy_count <= '24')) {
			$size = 'btn-m';
		} else {
			$size = 'btn-l';
		}

		if(get_field('text_alignment') == 'right') {
			$text_alignment = 'panel-r';
		} else {
			$text_alignment = '';
		}

		if(get_field('text_module_position') == 'right') {
			$module_pos = 'txt-mod-r';
		} else {
			$module_pos = 'txt-mod-l';
		}

		if($color) {
			$text_color = 'white';
		} else {
			$text_color = '';
		}

		if($overlay) {
			$use_overlay = 'overlay';
		} else {
			$use_overlay = '';
		}

		$img_full_s = wp_get_attachment_image_src( $img_full, 'full-panel-s');
		$img_full_m = wp_get_attachment_image_src( $img_full, 'full-panel-m');
		$img_full_l = wp_get_attachment_image_src( $img_full, 'full-panel-l');

		$img_half_s = wp_get_attachment_image_src( $img_half, 'half-panel-s');
		$img_half_l = wp_get_attachment_image_src( $img_half, 'half-panel-l');
	?>
		<section class="panel panel-dynamic flex-col <?php echo $bg; ?> <?php echo $text_alignment; ?> <?php echo $use_overlay; ?>">
			<div class="container-xl flex-row <?php echo $module_pos; ?>">
				<div class="panel-msg">
					<h2 class="headline-s <?php echo $text_color; ?>"><?php the_field('product_panel_copy'); ?></h2>
					<a href="<?php the_field('product_panel_cta_link'); ?>" class="btn <?php echo $size; ?>"><?php the_field('product_panel_cta_button'); ?></a>
				</div>	
			
				<?php if($img_half) : ?>
					<div class="animate-box <?php echo $module_pos; ?> ">
						<picture>
							<!--[if IE 9]><video style="display: none;"><![endif]-->
							<source media="(min-width: 87.5em)" srcset="<?php echo $img_half_l[0]; ?>" />
							<!--[if IE 9]></video><![endif]-->
							<img src="<?php echo $img_half_s[0]; ?>" alt="<?php the_title(); ?>" />
						</picture>
					</div>
				<?php elseif($img_full) : ?>
					<picture>
						<!--[if IE 9]><video style="display: none;"><![endif]-->
						<source media="(min-width: 87.5em)" srcset="<?php echo $img_full_l[0]; ?>" />
						<source media="(min-width: 50em)" srcset="<?php echo $img_full_m[0]; ?>" />
						<!--[if IE 9]></video><![endif]-->
						<img src="<?php echo $img_full_s[0]; ?>" alt="<?php the_title(); ?>" />
					</picture>
				<?php endif; ?>
			</div>
		</section>
	<?php endwhile; wp_reset_postdata(); ?>
<?php endif; ?>