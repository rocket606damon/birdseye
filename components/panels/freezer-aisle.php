<section class="panel panel-freezer-aisle">
	<div class="container-site flex-col">
		<div class="panel-msg">
			<h2 class="headline-s"><?php the_field('freezer_aisle_title'); ?></h2>
			<p><?php the_field('freezer_aisle_copy'); ?></p>
			<a href="<?php the_field('cta_button_link_freezer_aisle'); ?>" class="btn btn-m"><?php the_field('cta_button_freezer_aisle'); ?></a>
		</div>	
	</div>
</section>