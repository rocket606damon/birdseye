<section class="panel panel-flavor-full new-panel flex-col">
	<div class="panel-msg offset container-site">
		<div class="presentation bird-left">
			<picture>
				<!--[if IE 9]><video style="display: none;"><![endif]-->
				<source media="(min-width: 50em)" srcset="<?php echo get_template_directory_uri(); ?>/assets/images/global/bird-new-flavor-full@2x.png" />
				<!--[if IE 9]></video><![endif]-->
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/global/bird-new-flavor-full.png" alt="" />
			</picture>
		</div>
		<h2 class="headline-xl white">Mind Blowing Flavors</h2>
		<p class="headline-s"><span>All New!</span> Flavor Full</p>
	</div>
</section>