<?php if(get_field('why_frozen_panel_copy')) : ?>
	<section class="panel panel-r panel-frozenfresh flex-col"
		data-center="background-position: 0 50%;"
		data-top-bottom="background-position: 0 45%;"
		data-bottom-top="background-position: 0 55%;"
		data-anchor-target=".panel-frozenfresh"
	>
		<div class="panel-msg container-site">
			<h2 class="headline-s"><?php the_field('why_frozen_panel_copy'); ?></h2>
			<a href="<?php the_field('cta_button_link_why_frozen'); ?>" class="btn btn-r"><?php the_field('cta_button_copy_why_frozen'); ?></a>
		</div>
		<div class="animate-box broccoli">
			<picture>
				<!--[if IE 9]><video style="display: none;"><![endif]-->
				<source media="(min-width: 87.5em)" srcset="<?php echo get_template_directory_uri(); ?>/assets/images/panels/bg-broccoli-s.jpg" />
				<!--[if IE 9]></video><![endif]-->
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/panels/bg-broccoli-l.jpg" alt="Frozen vegetables are as healthy as fresh vegetables" />
			</picture>
		</div>
	</section>
<?php endif; ?>