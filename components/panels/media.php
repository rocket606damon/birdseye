<section class="panel media-panel flex-col">
	<header class="h-pad container-s">
		<?php if(get_field('vp_title')) : ?>
			<h2 class="headline-m"><?php the_field('vp_title'); ?></h2>
		<?php endif; ?>
		<?php if(get_field('vp_copy')) : ?>
			<?php
				if(get_field('vp_copy_align')) {
					$align = 'left';
				} else {
					$align = '';
				}
			?>
			<p class="panel-copy no-break <?php echo $align; ?>"><?php the_field('vp_copy'); ?></p>
			<?php endif; ?>
				<?php if(get_field('cta_link_external')) : ?>
				<a href="<?php the_field('cta_link_external'); ?>" class="btn btn-c btn-l" target="_blank"><?php the_field('vp_cta_text'); ?></a>
			<?php elseif(get_field('cta_link_internal')) : ?>
				<a href="<?php the_field('cta_link_internal'); ?>" class="btn btn-c btn-l"><?php the_field('vp_cta_text'); ?></a>
			<?php endif; ?>
	</header>
	
	<?php if(have_rows('video_repeater')) : ?>
		<?php $i = count(get_field('video_repeater')); ?>

		<?php if($i == 1) : // if only one video ?>

			<?php while(have_rows('video_repeater')) : the_row(); ?>
				<?php
					$cover_img = get_sub_field('cover_image');
					$img_s = wp_get_attachment_image_src($cover_img, 'video-cover-s');
					$img_l = wp_get_attachment_image_src($cover_img, 'video-cover-l');
				?>
				<div class="container-media single-asset video-container">
					<div class="video-cover play">
						<picture>
							<!--[if IE 9]><video style="display: none;"><![endif]-->
							<source media="(min-width: 50em)" srcset="<?php echo $img_l[0]; ?>" />
							<!--[if IE 9]></video><![endif]-->
							<img src="<?php echo $img_s[0]; ?>" alt="" />
						</picture>
					</div>
					<?php //the_sub_field('video'); ?>
					<?php 
						if(get_sub_field('video')) {
							the_sub_field('video');
						}
					?>	
				</div>
			<?php endwhile; ?>

		<?php elseif($i > 1) : ?>
			<div class="container-media">
				<?php 
					$video_layout = get_field('videos_layout');
					if($video_layout == 'slider') {
							$video_display = "media-slider";
					} else if($video_layout == 'thumbs') {
							$video_display = "media-slider thumbs";
					} else if($video_layout == 'stacked') {
							$video_display = "stacked-videos";
					}

					$thumbs = get_field('video_thumbs');
					if($thumbs) {
						$thumbnails = 'thumbs';
					} else {
						$thumbnails = '';
					}

					if(($i == 3) || ($i == 5)) {
						$slides = 'thirds';
					} else if($i == 2) {
						$slides = 'two'; 
					} else {
						$slides = '';
					}
				?>
				<ul class="<?php echo $video_display; ?> <?php echo $slides; ?>">
					<?php 
						while(have_rows('video_repeater')) : the_row(); 
							
							$video_title = get_sub_field('video_title');
							$cover_img = get_sub_field('cover_image');
							$thumbnail = wp_get_attachment_image_src($cover_img, 'video-thumb');
							$img_s = wp_get_attachment_image_src($cover_img, 'video-cover-s');
							$img_l = wp_get_attachment_image_src($cover_img, 'video-cover-l');
					?>
						<?php if($video_layout == 'thumbs') : ?>
							<li class="video-container" data-thumb="<?php echo $thumbnail[0]; ?>" data-title="<?php echo $video_title; ?>">
						<?php else : ?>
							<li class="video-container">
						<?php endif; ?>
							<?php if($cover_img) : ?>
								<div class="video-cover play">
									<picture>
										<!--[if IE 9]><video style="display: none;"><![endif]-->
										<source media="(min-width: 50em)" srcset="<?php echo $img_l[0]; ?>" />
										<!--[if IE 9]></video><![endif]-->
										<img src="<?php echo $img_s[0]; ?>" alt="">
									</picture>
								</div>
								<?php endif; ?>
							<?php the_sub_field('video'); ?>
						</li>
					<?php endwhile; ?>
				</ul>
			</div>
		<?php endif; ?>
	<?php endif; ?>
</section>