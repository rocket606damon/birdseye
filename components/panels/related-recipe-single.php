	<?php
		$img_field = get_field('recipe_image');
		$recipe_img = wp_get_attachment_image_src($img_field, 'recipe');

		echo '<article class="recipe-excerpt">';
		echo '<a href="' . get_permalink() . '">';
		echo '<img src="" data-src="' . $recipe_img[0] . '" class="lazy">';
		echo '<div class="flex-col"><h3 class="prod-feature-title bold">' . get_the_title() . '</h3>';
		echo '</div></a></article>';
	?>