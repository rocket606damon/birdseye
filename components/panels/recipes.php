<?php 
	$posts = get_field('recipe_panel'); 
	if($posts) : 
?>
	<section id="relatedRecipes" class="panel hp-f flex-col">
		<header class="related-recipe-header">
			<?php if(get_field('recipe_panel_title')) : ?>
				<h2 class="headline-s"><?php the_field('recipe_panel_title'); ?></h2>
			<?php else : ?>
				<h2 class="headline-s">Recipe Suggestions</h2>
			<?php endif; ?>
			<?php if(get_field('recipe_panel_sub')) : ?>
				<p class="support-copy"><?php the_field('recipe_panel_sub'); ?></p>
			<?php endif; ?>
		</header>
		<div class="container-site recipe-container flex-row">
			<?php foreach ($posts as $post) : setup_postdata($post); ?>
				<?php
					$img = get_field('recipe_image');
					$recipe_img = wp_get_attachment_image_src($img, 'recipe');
				?>
				<article class="recipe-excerpt">
					<a href="<?php the_permalink(); ?>">
						<img src="" data-src="<?php echo $recipe_img[0]; ?>" class="lazy">
						<div class="flex-col">
							<h3 class="prod-feature-title bold"><?php the_title(); ?></h3>	
						</div>
					</a>
				</article>
			<?php endforeach; wp_reset_postdata(); ?>
		</div>
	</section>
<?php else : 
	$recipes = new WP_Query(
		array(
			'post_type' => 'recipe',
			'posts_per_page' => 3,
			'post_status' => 'publish',
			'orderby' => 'rand'
		)
	);
?>

	<section id="relatedRecipes" class="panel hp-f flex-col">
		<header class="related-recipe-header">
			<?php if(get_field('recipe_panel_title')) : ?>
				<h2 class="headline-s"><?php the_field('recipe_panel_title'); ?></h2>
			<?php else : ?>
				<h2 class="headline-s">Recipe Suggestions</h2>
			<?php endif; ?>
			<?php if(get_field('recipe_panel_sub')) : ?>
				<p class="support-copy"><?php the_field('recipe_panel_sub'); ?></p>
			<?php endif; ?>
		</header>
		<div class="container-site recipe-container flex-row">
			<?php while($recipes->have_posts()) : $recipes->the_post();  ?>
				<?php
					$img = get_field('recipe_image');
					$recipe_img = wp_get_attachment_image_src($img, 'recipe');
				?>
				<article class="recipe-excerpt">
					<a href="<?php the_permalink(); ?>">
						<img src="" data-src="<?php echo $recipe_img[0]; ?>" class="lazy">
						<div class="flex-col">
							<h3 class="prod-feature-title bold"><?php the_title(); ?></h3>	
						</div>
					</a>
				</article>
			<?php endwhile; wp_reset_postdata(); ?>
		</div>
	</section>

<?php endif; ?>