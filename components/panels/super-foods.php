<section class="panel panel-super-foods new-panel flex-col">
	<div class="panel-msg offset container-site">
		<div class="presentation bird-right">
			<picture>
				<!--[if IE 9]><video style="display: none;"><![endif]-->
				<source media="(min-width: 50em)" srcset="<?php echo get_template_directory_uri(); ?>/assets/images/global/bird-new-super-foods@2x.png" />
				<!--[if IE 9]></video><![endif]-->
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/global/bird-new-super-foods.png" alt="" />
			</picture>
		</div>
		<h2 class="headline-xl white">Out of This World Good</h2>
		<p class="headline-s"><span>All New!</span> Super Foods Blends</p>
	</div>
</section>