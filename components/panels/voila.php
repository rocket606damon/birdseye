<section class="panel panel-voila flex-col">
	<div class="container-site flex-row">
		<div class="panel-msg feature-voila">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/images/product/birdseye-voila-logo@2x.png">
			<?php if(get_field('voila_panel_lead')) : ?>
				<p class="prod-feature-title bold"><?php the_field('voila_panel_lead'); ?></p>
			<?php endif; ?>
			<h2 class="headline-s btm-mrg"><?php the_field('headline_voila'); ?></h2>
			<?php if(get_field('support_copy_voila')) : ?>
				<p class="prod-feature-title light alt-space"><?php the_field('support_copy_voila'); ?></p>
			<?php endif; ?>
			<a href="<?php the_field('cta_button_link_voila'); ?>" class="btn btn-l"><?php the_field('cta_button_copy_voila'); ?></a>
		</div>
	</div>
	<div class="animate-box voila-bowl"
		data-bottom-top="margin-top: -6%"
		data-top-bottom="margin-top: 6%"
		data-anchor-target=".panel-voila"
	>
		<picture>
			<!--[if IE 9]><video style="display: none;"><![endif]-->
			<source media="(min-width: 50em)" srcset="<?php echo get_template_directory_uri(); ?>/assets/images/product/voila-panel-alfredo-chicken-s.jpg" />
			<!--[if IE 9]></video><![endif]-->
			<img src="<?php echo get_template_directory_uri(); ?>/assets/images/product/voila-panel-alfredo-chicken-l.jpg" alt="Birds Eye Voila skillet meal" />
		</picture>
	</div>
</section>