<section class="panel panel-r panel-products-all flex-col">
	<div class="panel-msg container-s">
		<h2 class="headline-s">Vegetable Nutrition<br>Made Easy</h2>
		<a href="<?php echo home_url('/our-products'); ?>" class="btn btn-r">Our Products</a>
	</div>
</section>