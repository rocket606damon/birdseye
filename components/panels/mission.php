<?php if(get_field('mission_panel_copy')) : ?>
	<section class="panel panel-mission flex-col">
		<div class="panel-msg container-site">
			<h2 class="headline-s"><?php the_field('mission_panel_copy'); ?></h2>
			<a href="<?php the_field('cta_button_link') ?>" class="btn btn-l"><?php the_field('cta_button_copy'); ?></a>
		</div>
	</section>
<?php endif; ?>