<section class="panel panel-roots flex-col">
	<div class="bg"></div>
	<div class="panel-msg container-site">
		<h2 class="headline-s white"><?php the_field('roots_panel_copy'); ?></h2>
		<a href="<?php the_field('cta_button_link_roots'); ?>" class="btn btn-l"><?php the_field('cta_button_copy_roots'); ?></a>
	</div>
</section>