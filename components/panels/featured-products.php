<?php 
	$posts = get_field('product_selection');
	if($posts) : 
?>
	<section class="panel panel-c panel-products-feature extra-pad flex-col">
		<div class="container-site">
			<?php if(get_field('featured_prod_headline')) : ?>
				<header>
					<h2 class="headline-s"><?php the_field('featured_prod_headline'); ?></h2>
					<?php if(get_field('featured_supporting_copy')) : ?>
						<p class="support-copy"><?php the_field('featured_supporting_copy'); ?></p>
					<?php endif; ?>
				</header>
			<?php endif; ?>

			<?php 
				foreach ($posts as $post) : setup_postdata($post);
					if(get_field('new_product')) {
						$is_new = 'is-new';
					} else {
						$is_new = 'no-new';
					}

			?>
			<?php endforeach; wp_reset_postdata(); ?>

			<div class="prod-feature <?php echo $is_new; ?>">
				<?php foreach ($posts as $post) : setup_postdata($post); ?>
					<?php 
						if(get_field('new_product')) {
							$new = 'new';
						} else {
							$new = '';
						}
					?>
					<a href="<?php the_permalink(); ?>" class="featured <?php echo $new; ?> flex-col">
						<div class="img-wrap">
							<?php the_post_thumbnail('post-thumbnail', array('title' => get_the_title(), 'alt' => get_the_title())); ?>
						</div>
						<div class="copy-wrap flex-col">
							<h3 class="prod-feature-title"><?php the_title(); ?></h3>
						</div>
					</a>
				<?php endforeach; wp_reset_postdata(); ?>
			</div>
			<?php 
				$cta_copy = get_field('cta_button_copy_featured');
				$copy_count = strlen($cta_copy);
				
				if($copy_count <= '18') {
					$size = '';
				} else if(($copy_count >= '19') && ($copy_count <= '24')) {
					$size = 'btn-m';
				} else {
					$size = 'btn-l';
				}
				if($cta_copy) :
			?>
				<a href="<?php the_field('cta_button_link_featured'); ?>" class="btn btn-c <?php echo $size; ?>"><?php the_field('cta_button_copy_featured'); ?></a>
			<?php endif; ?>
		</div>
	</section>
<?php else : 
	$prods = new WP_Query(
		array(
			'post_type' => 'product',
			'posts_per_page' => 4,
			'post_status' => 'publish',
			'orderby' => 'rand'
		)
	);
?>
	<section class="panel panel-c panel-products-feature extra-pad flex-col">
		<div class="container-site">
			<header>
				<?php if(get_field('featured_prod_headline')) : ?>
					<h2 class="headline-s"><?php the_field('featured_prod_headline'); ?></h2>
				<?php else : ?>
						<h2 class="headline-s">Other Products</h2>
				<?php endif; ?>
					<?php if(get_field('featured_supporting_copy')) : ?>
						<p class="support-copy"><?php the_field('featured_supporting_copy'); ?></p>
					<?php endif; ?>
			</header>
			
			<div class="prod-feature">
				<?php while($prods->have_posts()) : $prods->the_post();  ?>
					<?php 
						if(get_field('new_product')) {
							$new = 'new';
						} else {
							$new = '';
						}
					?>
					<a href="<?php the_permalink(); ?>" class="featured <?php echo $new; ?> flex-col">
						<div class="img-wrap">
							<?php the_post_thumbnail('post-thumbnail', array('title' => get_the_title(), 'alt' => get_the_title())); ?>
						</div>
						<div class="copy-wrap flex-col">
							<h3 class="prod-feature-title"><?php the_title(); ?></h3>	
						</div>
					</a>
				<?php endwhile; wp_reset_postdata(); ?>
			</div>
			<?php 
				$cta_copy = get_field('cta_button_copy_featured');
				$copy_count = strlen($cta_copy);
				
				if($copy_count <= '18') {
					$size = '';
				} else if(($copy_count >= '19') && ($copy_count <= '24')) {
					$size = 'btn-m';
				} else {
					$size = 'btn-l';
				}
				if($cta_copy) :
			?>
				<a href="<?php the_field('cta_button_link_featured'); ?>" class="btn btn-c <?php echo $size; ?>"><?php the_field('cta_button_copy_featured'); ?></a>
			<?php endif; ?>
		</div>
	</section>
<?php endif; ?> 
