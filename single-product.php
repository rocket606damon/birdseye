<?php
/** 
 *   single product pg
 */

//kd_enqueue_stylesheet('product');
get_header(); ?>


<?php

$hero_image = get_field('hero_image');
$product_logo = get_field('product_logo');

if ($hero_image == '' || $product_logo == '') {

	$terms = get_the_terms($post->ID, 'product_category');
	$parent = $terms[0]->parent;

	if ($parent != 0 && $parent != NULL) {
		while( $parent != 0 ) {
			$term_id = get_term($parent, 'product_category')->term_id;
			$parent = get_term($parent, 'product_category')->parent;
		}
	} else {
		$term_id = $terms[0]->term_id;
	}

	//$term_id = $terms[0]->term_id;
//var_dump($terms); 

	if ($hero_image == '') {
		$hero_image = get_field('hero_image', 'term_' . $term_id);
		// var_dump($term_id); 40
	}

	if ($product_logo == '') {
		$product_logo = get_field('product_logo', 'term_' . $term_id);
	}

	if ($hero_image == '') {
		$hero_image = ');background-image: linear-gradient(90deg, #03BAE6 1%, #21547F 99%);height:7em;';
		$product_logo = '';
	}
}

?>

<section class="hero-products hero-products-single" style="background-image:url(<?php echo $hero_image; ?>);">
	<div class="container-site flex-row">
		<div class="hero-msg">
			<picture>
				<!--[if IE 9]><video style="display: none;"><![endif]-->
				<source media="(min-width: 50em)" srcset="<?php echo $product_logo; ?>" />
				<!--[if IE 9]></video><![endif]-->
				<img srcset="<?php echo $product_logo; ?>" alt="" />
			</picture>
		</div>
	</div>
</section>


	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<?php include('components/panels/product-info.php'); ?>
	<?php endwhile; endif; ?>

<?php
	include('components/panels/other-varieties.php'); 
	include('components/panels/product-panel-dynamic.php');
	include('components/panels/recipes.php'); 
	include('components/panels/featured-products.php'); 
?>

<?php
get_footer();
