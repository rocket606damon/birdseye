<?php
/* Template Name: Frozen v Fresh Pg */

//kd_enqueue_stylesheet('frozenfresh');
get_header(); ?>

<section class="panel frozen-intro pre-load">
	<h1 class="headline-block lead-70">
		<span class="cap" style="font-size:40%;margin-bottom: 0.29em;">Yes, Frozen Vegetables</span>
		<span class="sans cap lines lines-frozen blue" style="font-size:25.7%;margin:-0.5em 0 0.5em;">Are As</span>
		<span style="margin-bottom:0.25em;">Nutritious</span>
		<span class="cap" style="font-size:40%;">As Fresh Vegetables</span>
	</h1>

	<span class="frozen-bg tomatoes"></span>
	<span class="frozen-bg broccoli"></span>
	<span class="frozen-bg beans"></span>
</section>

<section class="frozen-benefits flex-row scene-1">
	<div class="flex-child move">
		<div class="frozen-msg">
			<h2 class="headline-f">Sourced from Local Farmers</h2>
			<p>Birds Eye veggies are not processed. They are grown with <br>care by real farmers. Our farmers carefully select only the<br> best, highest-quality vegetables.</p>
			<a href="<?php echo home_url(); ?>/our-story/our-farms" class="btn">See our Farms</a>
		</div>	
	</div>
	<div class="scene-number flex-child"><span class="big-number big-1">1</span></div>
	<div class="guide guide-1"></div>
</section>

<div class="scene-1-spacer"></div>

<section class="frozen-benefits flex-row scene-1-bg">
	<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/frozenvsfresh/Birdseye-Woman-farmer.png" class="woman-farmer">
</section>

<section class="frozen-benefits flex-row scene-2">
	<div class="flex-child">
		<div class="frozen-msg">
			<h2 class="headline-f">Picked at the Peak of Freshness</h2>
			<p>Our veggies ripen in the field until they are picked at their <br>peak, unlike many fresh vegetables that ripen on the way to <br>the grocery store. Our veggies contain no artificial colors or <br>artificial flavors. They are simply fresh vegetables, flash frozen!</p>
		</div>
	</div>
	<div class="scene-number flex-child"><span class="big-number big-2">2</span></div>
	<div class="guide guide-2"></div>
	<div class="corn-leaf"></div>
</section>


<section class="frozen-benefits flex-row scene-3">
	<div class="flex-child move">
		<div class="frozen-msg">
			<h2 class="headline-f">Flash Frozen to Lock in Nutrients</h2>
			<p>Birds Eye vegetables are flash frozen within hours of<br> harvest, locking in their nutrients. The cold temperature <br>preserves the quality, just as they were when they were first picked, <br>without the need for preservatives. In fresh vegetables, <br>nutrients are lost in shipping and shelving.</p>
		</div>
	</div>
	<div class="scene-number flex-child"><span class="big-number big-3">3</span></div>
	<div class="guide guide-3"></div>
	<div class="scene-3-dish"></div>
</section>



<section class="frozen-benefits flex-row scene-4">
	<div class="flex-child">
		<div class="frozen-msg">
			<h2 class="headline-f">In Season. All Year.</h2>
			<p>Frozen vegetables are always in season, which makes nutritious <br>meal planning a snap. Even better, our flash-frozen veggies taste <br>as fresh as they did the moment they were picked.</p>
		</div>	
	</div>
	<div class="scene-number flex-child"><span class="big-number big-4">4</span></div>
</section>
<section class="fvf-statement bg-gradient">
	<div class="container-site">
		<h2 class="headline-xl white">Less Waste, <br>More Yum</h2>
		<p class="headline-f light">As much as 40% of our food supply is discarded each <br>year. Frozen foods last longer than fresh, so you can <br>use what you need and save the rest for later.</p>	
	</div>
</section>

<?php
	include('components/panels/featured-products.php');

	get_footer(); 