<?php
/* Template Name: Products Export */


$query = new WP_Query( 
	array(
		'post_type' => 'product',
		'posts_per_page' => 2,
		'post_status' => 'publish'
	)
);

$rows = array();
$titles = array();

if ($query->have_posts()) {

	while ($query->have_posts()) {

		$query->the_post();
		$row = array();

		$row[0] = '"' . get_the_title() . '"';
		$row[1] = '"' . get_permalink() . '"';
		$row[2] = '"' . get_the_content() . '"';

		$categories = get_the_terms( get_the_ID(), 'product_category' ); 
		if ( $categories && ! is_wp_error( $category ) ) : 

			// loop through each cat
			foreach($categories as $category) :

				if ( $category->parent != 0 && count($category->children) == 0) {
					$cat_child = $category->name;
				} else {
					$cat_parent = $category->name;
				}

			endforeach;

		endif;

		$row[3] = '"' . $cat_parent . '"';
		$row[4] = '"' . $cat_child . '"';


		$categories = get_the_terms( get_the_ID(), 'vegetables' ); 
		if ( $categories && ! is_wp_error( $category ) ) : 

			$veg = array();

			// loop through each cat
			foreach($categories as $category) :

				$veg[] = $category->name;

			endforeach;

		endif;

		$veg = implode(',', $veg);
		$row[5] = '"' . $veg . '"';

		$row[6] = '"' . get_field('upc') . '"';

		$rows[] = implode(',', $row);
		//echo implode(',', $row);
		//echo '<br>';

	}

}

?>
<!DOCTYPE html>
<html>
<head></head>
<body>
<pre>
<?php 

//	var_dump($rows);
echo implode("\n", $rows);

?>
</pre>
</body>
</html>