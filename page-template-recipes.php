<?php

/* Template Name: Recipes Listing Page */

// Get parent terms
$terms = get_terms( array(
	'taxonomy' => 'recipe_categories',
	'parent' => 0,
	'hide_empty' => false
));

$termlist = array();
foreach($terms as $term) {
	$termlist[] = $term;
}

get_header(); ?>

<section class="hero-recipes">
	<div class="hero-msg">
		<h1 class="headline-block lead-64">
			<span class="lines lines-recipes blue" style="font-size:56.25%;margin-bottom:0.25em;">Delicious Vegetable</span>
			<span>Recipes</span>
		</h1>
	</div>
</section>

<section class="flex-col">
	<div class="container-xl recipe-grid">
		<div class="filter-toggle">
			<span data-all="Filter Recipes" data-less="Close Filter">Filter Recipes</span>
		</div>
		<div class="product-filtering content-filter">
			<form id="product-search-form" class="product-search-form">
				<input type="text" id="product-search" placeholder="Search ...">
				<input type="submit" value="submit">
			</form>

			<div class="product-categories">
				<?php
					// Display parent terms
					for($a = 0; $a < count($termlist); $a++) {

						$hide = get_field('hide_category', 'term_' . $termlist[$a]->term_id);

						echo '<div class="parent" data-hide="' . $hide . '">';

							echo '<h1>' . $termlist[$a]->name . '</h1>';

							$categories = get_child_cats('recipe_categories', $termlist[$a]->term_id);

							display_category_hierarchy('recipe_categories', $categories, $termlist[$a]->term_id, 1); // Defined in functions.php
			
						echo '</div>';
					}
				?>
			</div>
			<span class="view-results btn btn-c show">View Results</span>
		</div>

		<div class="recipe-listing">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/global/ajax-loader.gif" style="width: 27px;height: 27.5px;display: block;margin: auto;">
		<?php
		/*
			$i = 1;
			$args = array(
				'post_type' => 'recipe',
				'posts_per_page' => -1,
				'post_status' => 'publish',
				'order' => 'ASC',
				'orderby' => 'title'
			);

			$recipe_query = new WP_Query( $args );
		?>
		<?php if ($recipe_query->have_posts()) : ?>
			<?php while ($recipe_query->have_posts() ) : $recipe_query->the_post(); ?>
				<?php
					$img_field = get_field('recipe_image');
					$recipe_img = wp_get_attachment_image_src($img_field, 'recipe');
				?>

				<article class="recipe-excerpt">
					<a href="<?php the_permalink(); ?>">
						<?php if($i > 4) : ?>
							<img src="" data-src="<?php echo $recipe_img[0]; ?>" class="lazy">	
						<?php else : ?>
							<img src="<?php echo $recipe_img[0]; ?>">
						<?php endif; ?>
						<div class="flex-col">
							<h3 class="prod-feature-title bold"><?php the_title(); ?></h3>	
						</div>
					</a>
				</article>
				<?php $i++; ?>
			<?php endwhile; wp_reset_postdata(); ?>
		<?php endif; */ ?>

		</div>

	</div>
</section>

<script id="single-recipes-template" type="text/x-handlebars-template">
{{#each posts}}
	<article class="recipe-excerpt">
		<a href="{{{permalink}}}">
			<img data-src="{{{image}}}" class="lazy">
			<div class="flex-col">
				<h3 class="prod-feature-title bold">{{{title}}}</h3>	
			</div>
		</a>
	</article>
{{/each}}
</script>

<?php
	get_footer();
