<?php
/* Template Name: Home Page */

//kd_enqueue_stylesheet('home');
get_header(); ?>

<section class="hero hero-video">
	<video id="hero-video" class="fullscreen-video" muted loop preload="auto">
		<source data-video="<?php echo get_stylesheet_directory_uri(); ?>/assets/videos/Birds-Eye-Home-Page-Hero-720.mp4" type="video/mp4">
		<source data-video="<?php echo get_stylesheet_directory_uri(); ?>/assets/videos/Birds-Eye-Home-Page-Hero-720.webm" type="video/webm">
	</video>	
	
	<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/home/BE_Logo_Mnumonic.png" class="be-logo" alt="Birds Eye logo">

	<h1 class="headline-block lead-64 white video-text">
		<span class="sans cap" style="font-size:31.25%;">It's Good to Eat</span>
		<span class="cap">Vegetables</span>
		<span class="sans cap lines lines-home" style="font-size:31.25%;margin-top:0.3em;">So We Make Vegetables</span>
		<span style="font-size:93.75%;">Good to Eat</span>
	</h1>
</section>

<?php 
	include('components/panels/mission.php'); 
	include('components/panels/featured-products.php'); 
	include('components/panels/frozen-fresh.php');
	include('components/panels/our-roots.php'); 
	include('components/panels/voila.php');
	include('components/panels/recipes.php');
	include('components/panels/media.php');
?>

<div class="be-intro">
	<video id="intro-vid" style="width:100%;position:absolute;z-index:999;top:0;right:0;display:none;">
		<source src="<?php echo get_stylesheet_directory_uri(); ?>/assets/videos/be-intro.mp4">
	</video>
</div>

<?php
	get_footer();