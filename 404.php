<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package birdseye
 */

get_header(); ?>

	<section class="general-content container-site error-404 not-found">
		<header class="post-header">
			<h1 class="headline-l" style="margin-bottom: 1em;">That page can't be found.</h1>
			<p style="text-align:center;">It looks like nothing was found at this location. Maybe try one of the links above, or <a href="<?php echo home_url(); ?>/contact">contact us</a>.</p>
		</header>
		<?php 
			while(have_posts()) : the_post();
				the_content();
			endwhile;
		?>
	</section>

<?php
get_footer();
