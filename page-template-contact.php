<?php
/* Template Name: Contact pg */

//kd_enqueue_stylesheet('frozenfresh');
get_header(); ?>

<section class="panel contact-intro">
	<h1 class="headline-s">We'd love to hear from you</h1>
	<p>We look forward to receiving your comments. We respect your privacy and will not share the information you provide here outside of our company. Because we frequently need to follow-up via telephone or postal mail, providing this information with your initial e-mail will help us serve you better.</p>
	<p>Although we love to hear your comments and encourage you to give us feedback about our products, please understand that we cannot accept suggestions for new products, advertising, promotions, etc. from anyone outside our company. This policy is necessary to prevent misunderstandings as to the origins of ideas.</p>
</section>
<section class="contact-form flex-row">
	<div class="form-wrapper">
		<p>Required fields *</p>
		<?php gravity_form(1, false, false, false, '', false); ?>
	</div>
	<aside class="contact-info">
		<h2>Consumer Contacts</h2>
		<ul>
			<li>Phone: <a href="tel:8883279060">888-327-9060</a></li>
		</ul>
		<h2>Investor Contacts</h2>
		<ul>
			<li><a href="mailto:investorrelations@pinnaclefoods.com" target="_blank">investorrelations@pinnaclefoods.com</a></li>
		</ul>
		<h2>Media Contacts</h2>
		<ul>
			<li><a href="mailto:mediainquiries@pinnaclefoods.com" target="_blank">mediainquiries@pinnaclefoods.com</a></li>
		</ul>
		<ul>
			<li>Having site-related problems?</li>
			<li><a href="techsupport@pinnaclefoods.com" target="_blank">techsupport@pinnaclefoods.com</a></li>
		</ul>
	</aside>
</section>


<?php
	get_footer(); 