jQuery(document).ready(function($){

	// Toggle the filter view
	$('.parent h1, .parent h4').click(function(e){
		$(this).toggleClass('show');
		$(this).siblings().toggleClass('show');
		e.stopPropagation();
	});

	var terms = [],
		search = '',
		ajaxTimeout = '',
		veg = [],
		time = window.Date.now();

	$('.term-id').change(function(e){
		clearTimeout(ajaxTimeout);
		ajaxTimeout = setTimeout(getFilteredRecipes, 500);
	});

	$('#product-search-form').submit(function(e){
		e.preventDefault();
		search = $('#product-search').val();
		getFilteredRecipes();
	});

	function recipeFilterURL() {
		var filter = [];
			//veg = [];

		$('.term-id:checked').each(function(){
			filter.push($(this).attr('id'));
		});

		// $('.veg-filters input:checked').each(function(){
		// 	veg.push($(this).attr('value'));
		// });

		window.history.pushState('', '', '?filter=' + filter.join('|'));// + '&veg=' + veg.join('|'));
	}

	function sortFilteredRecipes() {
		$('.term-id').each(function(){
			var id = $(this).attr('name');

			$('.single-product[data-catid="' + id + '"]').appendTo('.product-listing');
		});
		$('.single-product[data-catid=""]').appendTo('.product-listing');
	}

	function getFilteredRecipes() {
		terms = [];

		$('.term-id:checked').each(function(){
			terms.push($(this).attr('name'));
		});

		recipeFilterURL();

		var data = {
			"action": 	'get_recipes',
			"type": 	'get',
			"terms": 	terms,
			"search": 	search,
			"time": 	window.Date.now()
		};

		$('.recipe-listing').addClass('loading');

		$.ajax({
			url: "/wp-admin/admin-ajax.php",
			data: data,
			success: function(response) {
				
				$('.recipe-listing').removeClass('loading');

				if (response.posts && response.time > time) {
					time = response.time;
					var source   = $("#single-recipes-template").html();
					var template = Handlebars.compile(source);
					var html = template(response);

					$('.recipe-listing').html('');
					$('.recipe-listing').append(html);

					sortFilteredRecipes();
					setTimeout(refreshLazy, 100);
				}

			}
		});
	}

	search = window.location.search;
	if(search) {
		search = search.split('filter=')[1].split('&')[0].split('|');
		console.log(search);
		if (search.length > 0) {
			for (var a = 0; a < search.length; a ++) {
				$('[id="' + search[a] + '"]').prop('checked', true);
			}
		}   
	}

	getFilteredRecipes();

});
