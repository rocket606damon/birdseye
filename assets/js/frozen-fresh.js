// veggies
function scene1(scrollTop, vp_height) {
	var line = jQuery('.guide-1')[0].getBoundingClientRect(),
		height = (scrollTop + (vp_height / 2)) - jQuery('.guide-1').offset().top,
		big2 = jQuery('.big-2')[0].getBoundingClientRect(),
		max = 300,
		percent = scrollTop / max,
		opacity = 1,
		scale = 1;

	if (percent > 1) {
		percent = 1;
	}

	opacity *= (1 - percent);
	scale = (scale * (percent / 3)) + 1;

	//if (percent > 0) {
		jQuery('.frozen-bg.broccoli').css({'opacity': opacity, 'transform': 'scale(' + scale + ') translateZ(0)'});
		jQuery('.frozen-bg.tomatoes').css({'opacity': opacity, 'transform': 'scale(' + scale + ') translateZ(0)'});
		jQuery('.frozen-bg.beans').css({'opacity': opacity, 'transform': 'scale(' + scale + ') translateZ(0)'});
		jQuery('.frozen-intro .headline-block').css({'opacity': opacity, 'transform': 'scale(' + scale + ') translateZ(0)'});
	//`}

	if (percent == 1) {
		jQuery('.site-header').addClass('fvf-header-on');
	} else {
		jQuery('.site-header').removeClass('fvf-header-on');
	}

	// line
	if (line.top > (vp_height / 2)) {
		jQuery('.guide-1').removeClass('show');
	}
	if (line.top < (vp_height / 2) && big2.top > (vp_height / 2) + 20) {
		jQuery('.guide-1').css('height', height + 'px').addClass('show');
	}

}

// woman in field
function scene2(vp_height) {

	var womanfarmer = jQuery('.woman-farmer')[0].getBoundingClientRect(),
		field = jQuery('.scene-1-bg')[0].getBoundingClientRect(),
		max = 15,
		percent = 1 - (womanfarmer.bottom / (vp_height * 2)),
		transY = 10,
		bg = -50;

	if (percent > 1) {
		percent = 1;
	}

	transY *= percent;
	bg *= percent;

	if (womanfarmer.top < vp_height && womanfarmer.bottom > 0) {
		jQuery('.woman-farmer').css('transform', 'translateY(-' + transY + 'vh)');
		jQuery('.scene-1-bg').css('background-position', 'center bottom ' + bg + 'px');
	}

}

// corn on table
function scene3(vp_height) {

	var corn = jQuery('.scene-2')[0].getBoundingClientRect(),
		percent = (corn.bottom / (vp_height * 2)),
		percent2 = 1 - (corn.top / vp_height),
		bg = -150,
		border = 100,
		margin = -50;

	if (percent > 1) {
		percent = 1;
	}
	if (percent < 0) {
		percent = 0;
	}

	if (percent2 > 1) {
		percent2 = 1;
	}
	if (percent2 < 0) {
		percent2 = 0;
	}

	bg *= percent;
	border *= percent2;
	margin = (margin * percent2) + 10;

	if (corn.top < vp_height && corn.bottom > 0) {
		jQuery('.scene-2').css('background-position', 'center ' + bg + 'px');
		jQuery('.corn-leaf').css('top', bg + 'px');
	}

	if (corn.top < vp_height) {
		jQuery('.scene-2').css('border-top', border + 'px solid #fff');
		jQuery('.scene-2 .flex-child, .scene-2 .scene-number').css('margin-top', margin + 'px');
	}

}

// dinner plate
function scene4(vp_height) {
	var plate = jQuery('.scene-3-dish')[0].getBoundingClientRect(),
		text = jQuery('.scene-3 .flex-child')[0].getBoundingClientRect(),
		text2 = jQuery('.scene-4 .flex-child')[0].getBoundingClientRect(),
		percent = 1 - (plate.top / (vp_height)),
		top = -10;

	top = (top * percent) + 5;

	if (plate.top < vp_height && plate.bottom > 0) {
		jQuery('.scene-3-dish').css('top', top + 'em');
	}

	if (text.top + 200 < vp_height) {
		jQuery('.big-3').addClass('show');
	} else {
		jQuery('.big-3').removeClass('show');
	}

	if (text2.top + 200 < vp_height || (text.top < 150 && text2.top + 100 < vp_height) ) {
		jQuery('.big-4').addClass('show');
	} else {
		jQuery('.big-4').removeClass('show');
	}
}

// guy in field
function scene5(vp_height) {
	var field = jQuery('.scene-4')[0].getBoundingClientRect(),
		percent = 1 - (field.top / (vp_height)),
		margin = -70;

	margin *= percent;

	if (field.top < vp_height) {
		jQuery('.scene-4 .scene-number').css('margin-top', margin + 'px');
	}
	
}

// blue bar
function scene6(vp_height) {
	var bar = jQuery('.fvf-statement')[0].getBoundingClientRect(),
		max = 200,
		percent = (vp_height - (bar.top + 130)) / max,
		opacity = 1,
		trans = 25;

	if (percent > 1) {
		percent = 1;
	}

	opacity *= percent;
	trans = (1 - percent) * trans;

	if (bar.top < vp_height) {
		jQuery('.fvf-statement .headline-xl.white').css({ 'opacity': opacity, 'transform': 'translateX(-' + trans + 'px)' })
		jQuery('.fvf-statement .headline-f.light').css({ 'opacity': opacity, 'transform': 'translateX(' + trans + 'px)' })
	}
}

// products panel
function productLineup(vp_height) {
	var panelRect = jQuery('.panel-products-feature .container-site')[0].getBoundingClientRect(),
		max = 400,
		percent = ((vp_height - 50) - panelRect.top) / max,
		opacity = 1,
		outside = 25,
		inside = 12.5;

	if (percent > 1) {
		percent = 1;
	}

	opacity *= percent;
	outside *= (1 - percent);
	inside *= (1 - percent);

	if (percent > 0) {
		jQuery('.panel-products-feature .container-site').css('opacity', opacity);
		
		jQuery('.panel-products-feature .prod-feature a.featured').eq(0).css('transform', 'translateX(-' + outside + 'px)');
		jQuery('.panel-products-feature .prod-feature a.featured').eq(3).css('transform', 'translateX(' + outside + 'px)');

		jQuery('.panel-products-feature .prod-feature a.featured').eq(1).css('transform', 'translateX(-' + inside + 'px)');
		jQuery('.panel-products-feature .prod-feature a.featured').eq(2).css('transform', 'translateX(' + inside + 'px)');

	} else {
		jQuery('.panel-products-feature .container-site').css('opacity', 0);
	}
}


function animatePage() {
	var scrollTop = jQuery(window).scrollTop(),
		vp_height = jQuery(window).height(),
		vp_width = jQuery(window).width();

	if (vp_width >= 768) {
		scene1(scrollTop, vp_height);
		scene2(vp_height);
		scene3(vp_height);
		scene4(vp_height);
		scene5(vp_height);
		scene6(vp_height);
		productLineup(vp_height);
	}	
}

jQuery(document).scroll(animatePage);
jQuery(window).resize(animatePage);

jQuery(document).ready(function($){

	jQuery('.guide-1').css('transform', 'translateZ(0)');
	jQuery('.scene-1-bg').css('transform', 'translateZ(0)');
	jQuery('.scene-2').css('transform', 'translateZ(0)');
	jQuery('.corn-leaf').css('transform', 'translateZ(0)');
	jQuery('.scene-2 .flex-child, .scene-2 .scene-number').css('transform', 'translateZ(0)');
	jQuery('.scene-3-dish').css('transform', 'translateZ(0)');
	jQuery('.scene-4 .scene-number').css('transform', 'translateZ(0)');
	jQuery('.panel-products-feature .container-site').css('transform', 'translateZ(0)');

	animatePage();

	setTimeout(function(){
		$('.pre-load').addClass('load');
	}, 500);
	setTimeout(function(){
		$('.pre-load').removeClass('pre-load');
	}, 2000);
});