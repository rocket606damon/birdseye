jQuery(document).ready(function($) {

	var wtb_template,
			html,
			selected,
			source,
			container = $('.container-ps-catalog');

	$(document).one('ready', function() {
		var selected = 'BIRDS EYE STEAMFRESH';
		getPSCat(selected);
	})

	$('.wtb-checkbox input').on('click', function(e) {
			var count = $('.wtb-checkbox input:checked').length;
			if(count == 0) {
				$(container).html('');
				PriceSpider.rebind();
			} else {
				$(".wtb-checkbox input:checked").each(function() {
					if($('#wtbFilter').val() !=="") {
						$('#wtbFilter').val('');
					}

					selected = $(this).val();
				getPSCat(selected);
					if(!$($('.ps-widget')).hasClass(selected)) {
			  		
			  		//$('#wtbFilter').attr('placeholder', 'Search: '+ labels);
					}
			  	
			  });	
			}
			
	});

	

	function inputCheck(selected) {
		$(".wtb-checkbox input").each(function() {
			var selected = $(this).val(),
					labels = new Array;
	  	
	  	labels.push($(this).siblings('label').html());
	  	
	  	getPSCat(selected);

	  	$('#wtbFilter').attr('placeholder', 'Search: '+ labels);
	  });	
	}

	function getPSCat(selected) {
		$(container).addClass('loading').html('');
		var data = {
			'action': 'wtb_by_cat',
			'selection': selected,
			'security': jQuery('#nonce').attr('data-security'),
		};
		$.ajax({
      url: "/wp-admin/admin-ajax.php",
      data: data,
      type: 'post',
      dataType: 'json',
      success: function(response) {
        console.log(response);
        var source = $('#psCatResults'),
						wtb_template = Handlebars.compile($(source).html());

        results = wtb_template(response);

        $(container).removeClass('loading').prepend(results);
        PriceSpider.rebind();
      }
    });
	}

	$("#wtbFilter").keyup(function() {

			// Retrieve the input field text and reset the count to zero
			var filter = $(this).val(), count = 0;

			// Loop through the comment list
			// Note: this may just work with the new Burdseye site since ps-catalog is a class and not an ID.
			// 	the unmarked (no class or id) chain of concentric divs
			// 	around each product is fixed across brands we have encvountered.

			// find all framed products in the current PS page
			$("div.ps-container.ps-catalog div div div span span").each(function() {

				// get the text contents of each product
				var text = $(this).text();

				// The two valid text values are
				//	'No Product Image'
				//	'Where to Buy'
				// If the text is one of those then do run...
				if (text != 'No Product Image' && text != 'Where to Buy') {

					// output the text value (debug)
					//console.log(text);
					// Apply (simple) regex expression to check:
					//	does the current input set of characters match product?
					// If the list item does not contain the text phrase fade it out
					if ($(this).text().search(new RegExp(filter, "i")) < 0) {
						//$(this).fadeOut();
						$(this).parent().parent().parent().fadeOut().addClass('gone');

					// Show the list item if the phrase matches and increase the count by 1
					} else {
						$(this).parent().parent().parent().show();
						count++;
					}
				}
			});
		});

});