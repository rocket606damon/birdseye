jQuery(document).ready(function($) {

	// need a fallback for object-fit

	function objectFit() {	
		$(window).load(function() {
			$('.panel-dynamic.full-img-bg img').each(function() {
				var src = $(this).attr('srcset'),
						curSrc = $(this).prop('currentSrc'),
						fit = 'cover';
				
				$(this).parents('.panel-dynamic.full-img-bg').css({'background' : 'transparent url("'+ curSrc +'") no-repeat center center /'+fit });
				$(this).parents('picture').remove();				
			})
		})
	}

	// check if supported 
	if(!Modernizr.objectfit) {
		objectFit();
	}

	// check product panels for new product indicator. if exists, decrease top padding
	function newProd() {
		var $panel = $('.panel'),
				$prodPanel = $panel.hasClass('panel-products-feature');

		if($prodPanel) {
			$('.featured').each(function() {
				if($(this).hasClass('new')) {
					$(this).parents('.prod-feature').addClass('has-new');
				} else {
					$(this).parents('.prod-feature').addClass('no-new');
				}
			})
			
		} else {
		}
	}

	newProd();

});