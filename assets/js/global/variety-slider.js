jQuery(document).ready(function($){

	$('.panel-varieties .slider').slick({
		infinite: true,
		slidesToShow: 2,
		slidesToScroll: 2,
		dots: true,
		arrows: false,
		mobileFirst: true,
		responsive: [
			{
				breakpoint: 600,
				settings: {
					slidesToShow: 4,
					slidesToScroll: 4
				}
			},
			{
				breakpoint: 1056,
				settings: {
					slidesToShow: 4,
					slidesToScroll: 4,
					dots: false,
					arrows: true,
				}
			}
		]
	});

});