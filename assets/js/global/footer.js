jQuery(window).scroll(function(){
	var vp_height = jQuery(window).height(),
		vp_width = jQuery(window).width(),
		panelRect = jQuery('.site-footer .bg')[0].getBoundingClientRect(),
		max = 100,
		percent = 1 - ((panelRect.bottom - max) / (vp_height - max));
		bg = 50;

	if (vp_width < 600) {
		bg = 15;
	}

	if (percent > 1) {
		percent = 1;
	}

	bg *= percent;

	if (panelRect.bottom < vp_height) {
		jQuery('.site-footer .bg').css('background-position', '0px ' + bg + 'px');
	}
});