// lazy load images
// http://robinosborne.co.uk/2016/05/16/lazy-loading-images-dont-rely-on-javascript/
var lazy = [];
//registerListener('load', setLazy);
//registerListener('scroll', lazyLoad);
//registerListener('resize', lazyLoad);


function setLazy() {  
  lazy = document.getElementsByClassName('lazy');
  // console.log('Found ' + lazy.length + ' lazy images');
} 

function lazyLoad() {
  var vp_height = jQuery(window).height();

  for(var i=0; i<lazy.length; i++){
    if(isInViewport(lazy[i], vp_height)){
      if (lazy[i].getAttribute('data-src')){
          lazy[i].src = lazy[i].getAttribute('data-src');
          lazy[i].removeAttribute('data-src');
      }
    }
  }  
  cleanLazy();
}

function cleanLazy() {
  lazy = Array.prototype.filter.call(lazy, function(l){ return l.getAttribute('data-src');});
}

function isInViewport(el, vp_height) {
  var rect = el.getBoundingClientRect();

  if (rect.top < 0 && rect.bottom < 0 || vp_height + 400 < rect.top) {
    return false;
  } else {
    return true;
  }
    
  // return (
  //   rect.bottom >= 0 && 
  //   rect.right >= 0 && 
  //   rect.top <= (window.innerHeight || document.documentElement.clientHeight) && 
  //   rect.left <= (window.innerWidth || document.documentElement.clientWidth)
  // );
}
/*
function registerListener(event, func) {
  if (window.addEventListener) {
    window.addEventListener(event, func)
  } else {
    window.attachEvent('on' + event, func)
  }
}*/

function refreshLazy() {
  setLazy();
  lazyLoad();
}

jQuery(window).load(setLazy);
jQuery(window).scroll(lazyLoad);
jQuery(window).resize(lazyLoad);
