jQuery(document).ready(function($){

	$(function() {
		$('.filter-toggle').on('click', function() {
			var el = $('.filter-toggle span');
			$(this).toggleClass('toggled');
			$(this).siblings('.content-filter').toggleClass('filter-open');
			if($('.veg-filter-menu').hasClass('veg-open')) {
				$(this).removeClass('veg-open');
				$(this).parents('#flyout').removeClass('open');
			} else {
			}

			if($('#flyout').hasClass('open')) {
				$('#flyout').removeClass('open');
			} else {
			}

			el.text() == el.data("less") 
		    ? el.text(el.data("all")) 
		    : el.text(el.data("less"));
		})
	})

	$('#flyout p, #flyout').on('click', function(e) {
		$('#flyout').toggleClass('open');
		e.stopPropagation();
	})

	$('#filterClose').on('click', function() {
		$(this).parents('#flyout').removeClass('open');
	})

	$('.view-results').on('click', function() {
		var target = $(window).height(),
				resultsBtn = $('.view-results').offset().top,
				heroH = $('.hero-products').height() - 40,
				offset = resultsBtn - target;

		$(this).addClass('clicked');
		$(this).parents('.content-filter').removeClass('filter-open').siblings('.filter-toggle').removeClass('toggled');
		var el = $('.filter-toggle span');
		el.text() == el.data("less") 
		    ? el.text(el.data("all")) 
		    : el.text(el.data("less"));

		if(resultsBtn > target) {
			$('html, body').animate({ scrollTop: heroH }, 400);
		}
	})

	function viewResults() {
		var $btn = $('.view-results'),
				input = $('.product-filtering input'),
				checked = $('.product-filtering :checkbox:checked');

		$('.product-categories input').each(function() {
			if($(this).prop('checked')) {
				$btn.addClass('show');	
			} else {
				$btn.removeClass('show');
			}
		})
		$('.product-filtering input').change(function() {
			$btn.addClass('show');
		})

 	}
 	//viewResults();


 	var inview = function() {
		var resultsBtn = $('.view-results').offset().top,
				target = $(window).height();

				console.log('btn' + resultsBtn);
				console.log('window' + target);

		if(resultsBtn > target) {
			$('html, body').animate({ scrollTop: resultsBtn }, 400);
		}
	}


});