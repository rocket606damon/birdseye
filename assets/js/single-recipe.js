jQuery(document).ready(function($){

	$(window).load(function() {
		var recipeImg = $('.recipe-image'),
			imgH = $(recipeImg).height(),
			imgClone = $(recipeImg).clone().addClass('clone').removeClass('original-img').appendTo('.recipe'),
			cloneH = $(imgClone).height(),
			recipeH = $('.recipe-info').height(),
			offset = recipeH - imgH,
			vh = $(window).height(),
			vw = $(window).width(),
			pin = $('[data-pin-log]');

		$(window).on('scroll', function() {
			var recipeScroll = $(window).scrollTop();
			console.log(recipeScroll);
			console.log(offset);

			if(imgH > recipeH) {
				$(imgClone).addClass('not-fixed');
			} else if(offset < recipeScroll) {
				$(recipeImg).addClass('scroll');
				$(imgClone).addClass('go-away');
				$('.recipe [data-pin-log]').addClass('move-it');
			} else {
				$(recipeImg).removeClass('scroll');
				$(imgClone).removeClass('go-away');
				$('.recipe [data-pin-log]').removeClass('move-it');
			}
		}) 
	})
});