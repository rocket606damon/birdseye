var s = skrollr.init();

jQuery(document).ready(function($) {

	function stickynav() {
		var viewport = $(window).height(),
			scroll = $(window).scrollTop(),
			top = viewport - scroll;

		if (top - 80 <= 0) {
			top = 80;
			$('.site-header').addClass('sticky');
		} else {
			$('.site-header').removeClass('sticky');
		}

		$('.site-nav').css('top', top + 'px');
	}

	$(document).scroll(stickynav);
	$(window).resize(stickynav);
	stickynav();

});

function videoText(scrollTop) {
	var max = 80,
		percent = scrollTop / max,
		opacity = percent * 1,
		margin = (1 - percent) * 15;

	if (scrollTop <= max) {
		jQuery('.video-text').css('opacity', opacity);
		jQuery('.video-text span').eq(0).css('margin-bottom', margin + 'px');
		jQuery('.video-text span').eq(2).css({ 'margin-bottom': margin + 'px', 'margin-top': margin + 'px', });
	}

}

function logo() {
	var panelRect = jQuery('.panel-mission')[0].getBoundingClientRect(),
		max = 200,
		percent = panelRect.top / max,
		left = -200;

	if (percent > 1) {
		percent = 1;
	} else if (percent < 0) {
		percent = 0;
	}

	left = (left * percent) + 16;


	if (panelRect.top < 200) {
		jQuery('.site-logo').css('left', left + 'px');
	} else {
		jQuery('.site-logo').css('left', '-200px');
	}

}

function carrotPanel(vp_height) {
	var textRect = jQuery('.panel-mission .panel-msg')[0].getBoundingClientRect(),
		diff = vp_height - textRect.bottom,
		max = 150,
		percent = diff / max,
		opacity = percent * 1;

	if (opacity > 1) {
		opacity = 1;
	}

	if (textRect.bottom < vp_height) {
		jQuery('.panel-mission .panel-msg').css('opacity', opacity);
	} else if (textRect.bottom > vp_height) {
		jQuery('.panel-mission .panel-msg').css('opacity', 0);
	}

	var panelRect = jQuery('.panel-mission')[0].getBoundingClientRect(),
		max = 15,
		percent = 1 - (panelRect.bottom / (vp_height * 2)),
		transY = 30;

	if (percent > 1) {
		percent = 1;
	}

	transY *= percent;

	if (panelRect.top < vp_height && panelRect.bottom > 0) {
		jQuery('.panel-mission .panel-msg').css('transform', 'translateY(' + transY + 'vh)');
	}
}

function productLineup(vp_height) {
	var panelRect = jQuery('.panel-products-feature .container-site')[0].getBoundingClientRect(),
		max = 400,
		percent = ((vp_height - 50) - panelRect.top) / max,
		opacity = 1,
		outside = 25,
		inside = 12.5;

	if (percent > 1) {
		percent = 1;
	}

	opacity *= percent;
	outside *= (1 - percent);
	inside *= (1 - percent);

	if (percent > 0) {
		jQuery('.panel-products-feature .container-site').css('opacity', opacity);
		
		jQuery('.panel-products-feature .prod-feature a.featured').eq(0).css('transform', 'translateX(-' + outside + 'px)');
		jQuery('.panel-products-feature .prod-feature a.featured').eq(3).css('transform', 'translateX(' + outside + 'px)');

		jQuery('.panel-products-feature .prod-feature a.featured').eq(1).css('transform', 'translateX(-' + inside + 'px)');
		jQuery('.panel-products-feature .prod-feature a.featured').eq(2).css('transform', 'translateX(' + inside + 'px)');

	} else {
		jQuery('.panel-products-feature .container-site').css('opacity', 0);
	}
}

function broccoli(vp_height) {
	var panelRect = jQuery('.panel-frozenfresh')[0].getBoundingClientRect(),
		max = 15,
		percent = 1 - (panelRect.bottom / (vp_height * 2)),
		transY = 30;

	if (percent > 1) {
		percent = 1;
	}

	transY *= percent;

	if (panelRect.top < vp_height && panelRect.bottom > 0) {
		jQuery('.panel-frozenfresh .panel-msg').css('transform', 'translateY(' + transY + 'vh)');
	}
}

function roots(vp_height) {
	var panelRect = jQuery('.panel-roots')[0].getBoundingClientRect(),
		max = 15,
		percent = 1 - (panelRect.bottom / (vp_height * 2)),
		transY = -15;

	if (percent > 1) {
		percent = 1;
	}

	transY *= percent;

	if (panelRect.top < vp_height && panelRect.bottom > 0) {
		jQuery('.panel-roots .panel-msg').css('transform', 'translateY(' + transY + 'vh)');
		//jQuery('.panel-roots .bg').css('background-position', 'right 50% top ' + transY + 'vh');
		jQuery('.panel-roots .bg').css('transform', 'translateY(' + transY + 'vh)');
	}
}

function voila(vp_height) {
	var panelRect = jQuery('.panel-voila')[0].getBoundingClientRect();

	if (panelRect.top <= 0) {
		jQuery('.panel-roots .panel-msg, .panel-roots .bg').addClass('hide');
	} else {
		jQuery('.panel-roots .panel-msg, .panel-roots .bg').removeClass('hide');
	}
}

function panelText($panel, $text) {

	var panelRect = $panel[0].getBoundingClientRect(),
		textRect = $text[0].getBoundingClientRect(),
		diff = textRect.top - panelRect.top,
		opacity = diff / 100 || 0;

	if (panelRect.top <= textRect.top) {
		if (diff > 0 && diff <= 100) {
			$text.css('opacity', opacity);
			$text.addClass('animating')
				.removeClass('pre-animation')
				.removeClass('animation-end');
		}
		if (diff > 100) {
			$text.css('opacity', 1);	
			$text.addClass('animation-end')
				.removeClass('pre-animation')
				.removeClass('animating');
		}
	} else {
		$text.css('opacity', 0);
		$text.addClass('pre-animation')
			.removeClass('animation-end')
			.removeClass('animating');
	}

	if (panelRect.bottom < 0) {
		$text.addClass('out-of-view');
	} else {
		$text.removeClass('out-of-view');
	}

}

function veggieGoodVideo(vp_height) {
	var vid = jQuery('.video-cover')[1].getBoundingClientRect();

	if (jQuery('#intro-vid').is('.start') && vid.bottom <= vp_height && !jQuery('#intro-vid').is('.play')) {
		jQuery('#intro-vid').addClass('play');
		jQuery('#intro-vid')[0].play();
	}
}

function animatePage() {
	var scrollTop = jQuery(window).scrollTop(),
		vp_height = jQuery(window).height(),
		vp_width = jQuery(window).width();

	if (vp_width >= 768) {
		carrotPanel(vp_height);
		productLineup(vp_height);
		broccoli(vp_height);
		roots(vp_height);
		voila(vp_height);
		veggieGoodVideo(vp_height);
	}
}

jQuery(document).scroll(animatePage);


jQuery(document).ready(function($) {
	var scrollTop = jQuery(window).scrollTop(),
		vp_height = jQuery(window).height(),
		vp_width = jQuery(window).width();

	setTimeout(function(){
		$('.hero-video .headline-block').addClass('show');
		if (jQuery('.slick-current .fluid-width-video-wrapper').length > 0) {
			jQuery("#intro-vid")
				.appendTo('.slick-current .fluid-width-video-wrapper')
				.addClass('start')
				.show();
			veggieGoodVideo(vp_height);
		}
	}, 1000);

	animatePage();	

	jQuery("#intro-vid").click(function(){
		jQuery('#intro-vid').addClass('play');
		$(this)[0].play();
	});

	jQuery("#intro-vid").bind("ended", function() {
		jQuery('#intro-vid').fadeOut();
	});

});
