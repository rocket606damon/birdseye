jQuery(document).ready(function($){

	// Toggle the filter view
	$('.parent h1, .parent h4').click(function(e){
		$(this).toggleClass('show');
		$(this).siblings().toggleClass('show');
		e.stopPropagation();
	})

	$('.product-veg, .product-veg > p, .product-veg .close').click(function(e){
		$('.veg-filter-menu').toggle();
		e.stopPropagation();
	});
	$('html').click(function(e){
		$('.veg-filter-menu').hide();
	});
	$('.veg-filter-menu, .veg-filter-menu *').click(function(e){
		e.stopPropagation();
	});


	$('.veg-filter-buttons .select-all').click(function(){
		$('.veg-filter-menu input').prop('checked', true);
	});

	$('.veg-filter-buttons .clear').click(function(){
		$('.veg-filter-menu input').prop('checked', false);
	});

	// toggle product lineup
	$(function() {
		var lineup_toggle = $('.product-lineup .product-toggle');

			$(lineup_toggle).on('click', function() {
				$(this).toggleClass('toggled');
				$(this).parents('.product-lineup').toggleClass('reveal');
			});
	})

	var terms = [],
		search = '',
		ajaxTimeout = '',
		veg = [],
		time = window.Date.now();

	$('.term-id').change(function(e){
		clearTimeout(ajaxTimeout);
		ajaxTimeout = setTimeout(getFilteredProducts, 500);
	});

	$('#product-search-form').submit(function(e){
		e.preventDefault();
		search = $('#product-search').val();
		getFilteredProducts();
	});

	$('.veg-filters input').change(function(){
		veg = [];
		$('.veg-filters input:checked').each(function(){
			veg.push($(this).val());
		});
		clearTimeout(ajaxTimeout);
		ajaxTimeout = setTimeout(getFilteredProducts, 500);
	});

	function productFilterURL() {
		var filter = [],
			veg = [];

		$('.term-id:checked').each(function(){
			filter.push($(this).attr('id'));
		});

		$('.veg-filters input:checked').each(function(){
			veg.push($(this).attr('value'));
		});

		window.history.pushState('', '', '?filter=' + filter.join('|') + '&veg=' + veg.join('|'));
	}

	function sortFilteredProducts() {
		$('.term-id').each(function(){
			var id = $(this).attr('name');

			$('.single-product[data-catid="' + id + '"]').appendTo('.product-listing');
		});
		$('.single-product[data-catid=""]').appendTo('.product-listing');
	}

	function getFilteredProducts() {
		terms = [];

		$('.term-id:checked').each(function(){
			terms.push($(this).attr('name'));
		});

		productFilterURL();

		var data = {
			"action": 	'get_products',
			"type": 	'get',
			"terms": 	terms,
			"search": 	search,
			"veg": 		veg,
			"time": 	window.Date.now()
		};

		$('.product-listing').addClass('loading');

		$.ajax({
			url: "/wp-admin/admin-ajax.php",
			data: data,
			success: function(response) {

				$('.product-listing').removeClass('loading');
				
				if (response.posts && response.time > time) {
					time = response.time;
					var source   = $("#single-products-template").html();
					var template = Handlebars.compile(source);
					var html = template(response);

					$('.product-listing').html('');
					$('.product-listing').append(html);

					sortFilteredProducts();
					setTimeout(refreshLazy, 100);
				}
				var target = $(window).height();
			}
		});
	}

	search = window.location.search;
	if(search) {
		search = search.split('filter=')[1].split('&')[0].split('|');
		if (search.length > 0) {
			for (var a = 0; a < search.length; a ++) {
				$('[id="' + search[a] + '"]').prop('checked', true);
			}
		}   
	}
	search = window.location.search;
	if(search) {
		search = search.split('veg=')[1].split('&')[0].split('|');
		if (search.length > 0) {
			for (var a = 0; a < search.length; a ++) {
				$('[value="' + search[a] + '"]').prop('checked', true);
			}
		}   
	}

	getFilteredProducts();

	// back to top button
	$(function() {
		var hero = $('.hero-products').height(),
				filter = $('.product-filtering').height(),
				trigger = hero + (filter / 2),
				vw = $(window).width(),
				container = $('.prod-grid').width(),
				left = (vw - container) / 2 + 92.5,
				$up = $('#up');

				console.log(trigger);

		$(window).on('scroll', function(e) {
			var scrollPos = $(window).scrollTop();

			if(scrollPos > trigger) {
				$up.addClass('show').css('left', left);
			} else if(scrollPos < trigger) {
				$up.removeClass('show');
			}

			clearTimeout($.data(this, 'scrollCheck'));
	    $.data(this, 'scrollCheck', setTimeout(function() {
	      $up.addClass('no-scroll');
	    }, 350));

	    $(window).on('resize', function(e) {
				vw = $(window).width(),
				container = $('.prod-grid').width(),
				left = (vw - container) / 2 + 92.5,

				$('#up').css('left', left);
			});

		});

		$('#up').on('click', function() {
			$('html, body').animate( {
				scrollTop: 0
			}, 400);
		});
	});

});
