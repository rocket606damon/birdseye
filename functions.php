<?php
/**
 * birdseye functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package birdseye
 */

if ( ! function_exists( 'birdseye_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function birdseye_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on birdseye, use a find and replace
	 * to change 'birdseye' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'birdseye', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Main', 'birdseye' ),
		'secondary' => esc_html__( 'Footer', 'birdseye' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'birdseye_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );
}
endif;
add_action( 'after_setup_theme', 'birdseye_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function birdseye_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'birdseye_content_width', 640 );
}
add_action( 'after_setup_theme', 'birdseye_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function birdseye_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'birdseye' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'birdseye' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'birdseye_widgets_init' );

// custom image sizes
function birdseye_custom_image_setup () {
  add_theme_support( 'post-thumbnails' );
  add_image_size('video-thumb', 400, 225);
  add_image_size('video-cover-s', 800);
  add_image_size('video-cover-l', 1600);
  add_image_size('full-panel-s', 1000);
  add_image_size('full-panel-m', 1800);
  add_image_size('full-panel-l', 2400);
  add_image_size('half-panel-s', 800);
  add_image_size('half-panel-l', 1400);
  add_image_size('slider-l', 2400);
  add_image_size('slider-m', 1800);
  add_image_size('slider-s', 1000);
  add_image_size('recipe', 720, 576, array('center', 'top'));
  add_image_size('product-preview', 448);

  add_filter('image_size_names_choose', 'birdseye_custom_image_sizes');
}
add_action('after_setup_theme', 'birdseye_custom_image_setup');


/**
 * Enqueue scripts and styles.
 */
function birdseye_scripts() {
	wp_enqueue_style( 'birdseye-style', get_stylesheet_directory_uri() . '/assets/css/style.css' );

	// dequeue jquery, the re-register and load in footer
	wp_deregister_script('jquery');
	wp_register_script('jquery', includes_url('/js/jquery/jquery.js'), false, NULL, true);

	// wp-embeds.min.js no longer needed
	wp_deregister_script('wp-embed');

	// Register Scripts
	wp_register_script('birdseye-global-scripts', get_template_directory_uri() . '/assets/js/global.min.js', array('jquery'), '', true);
	wp_register_script('birdseye-products-listing', get_template_directory_uri() . '/assets/js/product-listing.js', false, NULL, true);
	wp_register_script('birdseye-product-info', get_stylesheet_directory_uri() . '/assets/js/product-info.js', false, NULL, true);
	wp_register_script('birdseye-recipe-listing', get_template_directory_uri() . '/assets/js/recipe-listing.js', false, NULL, true);
	wp_register_script('handle-bars', get_template_directory_uri() . '/assets/js/handlebars-v4.0.5.js', false, NULL, true);
	wp_register_script('tweenmax-js', 'https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.0/TweenMax.min.js', array('jquery'), '', true);
	wp_register_script('timelinelite-js', 'https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.0/TimelineLite.min.js', array('jquery'), '', true);
	wp_register_script('scroll-js', 'https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/ScrollMagic.min.js', array('jquery'), '',true);
	wp_register_script('scrollgsap-js', 'https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/animation.gsap.min.js', array('jquery'), '', true);
	wp_register_script('gsapdebug-js', 'https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/debug.addIndicators.min.js', array('jquery'), '', true);
	wp_register_script('skrollr-js', get_stylesheet_directory_uri() . '/assets/js/skrollr.js', array('jquery'), '', true);
	wp_register_script('home-animation-js', get_stylesheet_directory_uri() . '/assets/js/home-animation.js', array('jquery'), '', true);
	wp_register_script('custom-ease-js', get_stylesheet_directory_uri() . '/assets/js/CustomEase.min.js', array('jquery', 'tweenmax-js', 'timelinelite-js', 'scroll-js', 'scrollgsap-js', 'gsapdebug-js'), '', true);
	wp_register_script('video-js', get_stylesheet_directory_uri() . '/assets/js/video.js', array('jquery'), '', true);
	wp_register_script('hero-slider-js', get_stylesheet_directory_uri() . '/assets/js/hero-slider.js', array('jquery'), '', true);
	wp_register_script('frozen-fresh-js', get_stylesheet_directory_uri() . '/assets/js/frozen-fresh.js', array('jquery'), '', true);
	wp_register_script('mission-js', get_stylesheet_directory_uri() . '/assets/js/mission.js', array('jquery'), '', true);
	wp_register_script('newfrom-js', get_stylesheet_directory_uri() . '/assets/js/new-from-be.js', array('jquery'), '', true);
	wp_register_script('snow-js', get_stylesheet_directory_uri() . '/assets/js/snowfall.jquery.min.js', array('jquery'), '', true);
	wp_register_script('single-recipe-js', get_stylesheet_directory_uri() . '/assets/js/single-recipe.js', array('jquery'), '', true);
	wp_register_script('wtb-js', get_stylesheet_directory_uri() . '/assets/js/where-to-buy.js', array('jquery'), '', true);

	// Enqueue Scripts
	wp_enqueue_script('birdseye-global-scripts');

	if(is_page_template('page-template-home.php')) {
		//wp_enqueue_script('tweenmax-js');
		//wp_enqueue_script('timelinelite-js');
		//wp_enqueue_script('scroll-js');
		//wp_enqueue_script('scrollgsap-js');
		//wp_enqueue_script('gsapdebug-js');
		//wp_enqueue_script('custom-ease-js');
		wp_enqueue_script('skrollr-js');
		wp_enqueue_script('home-animation-js');
		wp_enqueue_script('video-js');
	}

	if (is_page_template('page-template-products-listing.php')) {
		wp_enqueue_script('handle-bars');
		wp_enqueue_script('birdseye-products-listing');		
	}

	if (is_page_template('page-template-recipes.php')) {
		wp_enqueue_script('handle-bars');
		wp_enqueue_script('birdseye-recipe-listing');		
	}

	if(is_page_template('page-template-new-from-birdseye.php')) {
		wp_enqueue_script('hero-slider-js');
		wp_enqueue_script('newfrom-js');
		wp_enqueue_script('snow-js');
	}

	if(is_page_template('page-template-our-roots.php')) {
		wp_enqueue_script('snow-js');
	}

	if(is_page_template('page-template-mission.php')) {
		wp_enqueue_script('mission-js');
		wp_enqueue_script('video-js');
	}

	if(is_page_template('page-template-frozen-fresh.php')) {
		wp_enqueue_script('frozen-fresh-js');
		//wp_enqueue_script('tweenmax-js');
		//wp_enqueue_script('timelinelite-js');
		//wp_enqueue_script('scroll-js');
		//wp_enqueue_script('scrollgsap-js');
	}

	if(is_singular('product') || is_singular('recipe')) {
		wp_enqueue_script('birdseye-product-info');
	}

	if(is_singular('recipe')) {
		wp_enqueue_script('single-recipe-js');
	}

	if(is_page_template('page-template-wtb.php')) {
		wp_enqueue_script('handle-bars');
		wp_enqueue_script('wtb-js');
	}

}
add_action('wp_enqueue_scripts', 'birdseye_scripts');

// load gravity scripts in footer
add_filter('gform_init_scripts_footer', '__return_true');

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

// remove emoji crap
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

// remove header crap
function remove_crap() {
	remove_action('wp_head', 'rsd_link');
	remove_action('wp_head', 'wp_generator');
	remove_action('wp_head', 'index_rel_link');
	remove_action('wp_head', 'wlwmanifest_link');
	remove_action('wp_head', 'feed_links', 2);
	remove_action('wp_head', 'feed_links_extra', 3);
	remove_action('wp_head', 'parent_post_rel_link', 10, 0);
	remove_action('wp_head', 'start_post_rel_link', 10, 0);
	remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);
	remove_action('wp_head', 'wp_shortlink_header', 10, 0);
	remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
}
add_action('init', 'remove_crap');

// pagination with page count ie; prev 1 / 3 next
function pagination_pg_count($numpages = '', $pagerange = '', $paged='') {
  if (empty($pagerange)) {
    $pagerange = 2;
  }
  global $paged;
  if (empty($paged)) {
    $paged = 1;
  }
  if ($numpages == '') {
    global $wp_query;
    $numpages = $wp_query->max_num_pages;
    if(!$numpages) {
        $numpages = 1;
    }
  }

	echo '<nav class="pagination container-xl">';
	echo '<div class="nav-previous">';
	echo get_next_posts_link('<span></span>&lt; Older Posts', $numpages);
	echo '</div>';
	echo '<div class="page-numbers">'.$paged.' / '.$numpages.'</div>';
	echo '<div class="nav-next">';
	echo get_previous_posts_link('Newer Posts &gt;');
	echo '</div>';
  echo '</nav>';
}


// recursive loop to display categories and products
function display_category_hierarchy($tax, $cats, $id, $level) {

	//display_products($cats, $id, $level);

	if ( !empty( $cats ) ) {

		foreach ($cats as $cat) {

			$subcategories = get_child_cats($tax, $cat->cat_ID, $tax);
			$hide = get_field('hide_category', 'term_' . $cat->cat_ID);

			echo '<div class="child" data-hide="'. $hide . '">';

				if (empty($subcategories)) {
					echo '<div class="checkbox-wrapper">';
					echo '<input class="term-id" type="checkbox" name="' . $cat->term_id .'" id="' . urlencode($cat->cat_name) . '-' . $cat->cat_ID . '">';
					echo '<label data-level="' . $level . '" for="' . urlencode($cat->cat_name) . '-' . $cat->cat_ID . '">' . $cat->cat_name . '</label>';
					echo '</div>';
				} else {
					echo '<h4 data-level="' . $level . '">' . $cat->cat_name . '</h4>';
				}

				display_category_hierarchy($tax, $subcategories, $cat->cat_ID, $level + 1);

			echo '</div>';

		}

	}

}

// returns any child categories
function get_child_cats($tax, $id='') {
	$args = array(
		'type'						=> 'product',
		'parent'					=> $id,
		'hide_empty'				=> 1,
		'taxonomy'					=> $tax,
		'pad_counts'				=> false 

	); 

	$categories = get_categories( $args );

	return $categories;
}

// displays the title of the products
function display_products($cats, $id, $level) {
	// exclude posts in sub-cats
	if ( !empty($cats) ) {
		$exclude = array();
		for ($b = 0; $b < count($cats); $b++) {
			$exclude[] = $cats[$b]->slug;
		}
	}

	$query = new WP_Query( array(
		'post_type' => 'product',
		'posts_per_page' => -1,
		'tax_query' => array(
			array(
				'taxonomy' => 'product_category',
				'field' => 'term_id',
				'terms' => $id
			),
			array(
				'taxonomy' => 'product_category',
				'terms' => $exclude,
				'field' => 'slug',
				'operator' => 'NOT IN',
			))
	));

	while ($query->have_posts()) : $query->the_post();

		echo '<p data-level="' . ($level - 1) . '"><a href="' . get_permalink() . '">' . get_the_title() . '</a></p>';

	endwhile;

	wp_reset_postdata();

}


// products listing page search
function get_products() {

	$response = array();
	$response['posts'] = array();
	$time = $_GET['time'];

	$terms = $_GET['terms'];
	$search = $_GET['search'];
	$veg = $_GET['veg'];

	if (!empty($terms) || !empty($veg)) {

		$tax_query = array();
		$tax_query['relation'] = 'OR';

		for($a = 0; $a < count($terms); $a++) {

			$tax_query[] =  array(
				'taxonomy' => 'product_category',
				'field' => 'term_id',
				'terms' => $terms[$a]
			);

		}

		for($b = 0; $b < count($veg); $b++) {

			$tax_query[] =  array(
				'taxonomy' => 'vegetables',
				'field' => 'term_id',
				'terms' => $veg[$b]
			);

		}

		$query = new WP_Query( 
			array(
				'post_type' => 'product',
				'posts_per_page' => -1,
				'post_status' => 'publish',
				'orderby' => 'title',
				'order' => 'ASC',
				's' => $search,
				'tax_query' => $tax_query
			)
		);

	} else {

		if ($search[0] == '') {

			$query = get_transient('all_prods_trans');

			if ($query == false) {

				$query = new WP_Query( 
					array(
						'post_type' => 'product',
						'posts_per_page' => -1,
						'post_status' => 'publish',
						'orderby' => 'title',
						'order' => 'ASC'
					)
				);

				set_transient('all_prods_trans', $query, DAY_IN_SECONDS);

			}

		} else {

			$query = new WP_Query( 
				array(
					'post_type' => 'product',
					'posts_per_page' => -1,
					'post_status' => 'publish',
					'orderby' => 'title',
					'order' => 'ASC',
					's' => $search
				)
			);

		}

	}
	
	if (empty($terms) && empty($veg) &&  $search[0] == '') {
		$data = get_transient('all_prods_data_trans');
		if ($data != '') {
			$data['time'] = $time;
			$data['trans'] = 'y';
			wp_send_json($data);
			die();
		}
	}

	if ($query->have_posts()) {

		while ($query->have_posts()) {

			$query->the_post();

			$temp = array();
			$temp['title'] = get_the_title();
			$temp['image'] = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'product-preview' )[0];
			$temp['new_product'] = get_field('new_product');
			$temp['permalink'] = get_permalink();

			$cat = '';
			$id = '';

			// get all product cats for the current post
			$categories = get_the_terms( get_the_ID(), 'product_category' ); 

			// wrapper to hide any errors from top level categories or products without category
			if ( $categories && ! is_wp_error( $category ) ) : 

				// loop through each cat
				foreach($categories as $category) :
					// get the children (if any) of the current cat
					if ( $category->parent != 0 && count($category->children) == 0) {
						$cat = $category->name;
						$id = $category->term_id;						
					}

				endforeach;

			endif;

			$temp['category'] = $cat;
			$temp['catID'] = $id;

			$response['posts'][] = $temp;		
			
		}
		
	}
	
	if (empty($terms) && empty($veg) &&  $search[0] == '') {
		set_transient('all_prods_data_trans', $response, DAY_IN_SECONDS);
	}

	$response['time'] = $time;
	$response['trans'] = 'n';
	wp_send_json($response);

}

add_action('wp_ajax_get_products', 'get_products');
add_action('wp_ajax_nopriv_get_products', 'get_products');



// recipe listing page search
function get_recipes() {

	$response = array();
	$response['posts'] = array();
	$time = $_GET['time'];

	$terms = $_GET['terms'];
	$search = $_GET['search'];

	if (!empty($terms)) {

		$tax_query = array();
		$tax_query['relation'] = 'OR';

		for($a = 0; $a < count($terms); $a++) {

			$tax_query[] =  array(
				'taxonomy' => 'recipe_categories',
				'field' => 'term_id',
				'terms' => $terms[$a]
			);

		}

		$query = new WP_Query( 
			array(
				'post_type' => 'recipe',
				'posts_per_page' => -1,
				'post_status' => 'publish',
				's' => $search,
				'tax_query' => $tax_query
			)
		);

	} else {

		$query = new WP_Query( 
			array(
				'post_type' => 'recipe',
				'posts_per_page' => -1,
				'post_status' => 'publish',
				's' => $search
			)
		);

	}
	
	if (empty($terms) && $search[0] == '') {
		$data = get_transient('all_recipes_data_trans');
		if ($data != '') {
			$data['time'] = $time;
			$data['trans'] = 'y';
			wp_send_json($data);
			die();
		}
	}

	if ($query->have_posts()) {

		while ($query->have_posts()) {

			$query->the_post();
			$temp = array();
			$temp['title'] = get_the_title();

			$img_field = get_field('recipe_image');
			$recipe_img = wp_get_attachment_image_src($img_field, 'recipe');

			$temp['image'] = $recipe_img[0];
			$temp['permalink'] = get_permalink();

			$cat = '';
			$id = '';

			// get all product cats for the current post
			$categories = get_the_terms( get_the_ID(), 'recipe_categories' ); 

			// wrapper to hide any errors from top level categories or products without category
			if ( $categories && ! is_wp_error( $category ) ) : 

				// loop through each cat
				foreach($categories as $category) :
					// get the children (if any) of the current cat
					$children = get_categories( array ('taxonomy' => 'recipe_categories', 'parent' => $category->term_id ));

					if ( count($children) == 0 ) {
						$cat = $category->name;
						$id = $category->term_id;
					}

				endforeach;

			endif;

			$temp['category'] = $cat;
			$temp['catID'] = $id;

			$response['posts'][] = $temp;			
			
		}
		
	}
	
	if (empty($terms) &&  $search[0] == '') {
		set_transient('all_recipes_data_trans', $response, DAY_IN_SECONDS);
	}

	$response['time'] = $time;
	$response['trans'] = 'n';
	wp_send_json($response);

}

add_action('wp_ajax_get_recipes', 'get_recipes');
add_action('wp_ajax_nopriv_get_recipes', 'get_recipes');


function bullets($atts, $content = null) {
	return '<div class="bullets">' .$content. '</div>';
}
function tight_list($atts, $content = null) {
	return '<div class="tight-list">' .$content. '</div>';
}

function register_shortcode() {
	add_shortcode('bullet_points', 'bullets');  
	add_shortcode('reduced_space_list', 'tight_list');  
}
add_action('init', 'register_shortcode');


function update_ps_cat() {
	if(isset($_POST['selection'])) {
		$ps_cat_selection = $_POST['selection'];

		$ps_cat_selection_str = implode(" ", $ps_cat_selection);

		$ps_cat_tag = strtoupper(str_replace('-', ' ', $ps_cat_selection));
		$ps_cat_lower = strtolower($ps_cat_selection);
	} else {
		print 'error';
	}
	
	$pscats = array();

	$pscat['val'] = $ps_cat_tag;
	$pscat['name'] = $ps_cat_lower;

	$pscats[] = $pscat;

	wp_send_json(array("pscats" => $pscats));
}
add_action('wp_ajax_wtb_by_cat', 'update_ps_cat');
add_action('wp_ajax_nopriv_wtb_by_cat', 'update_ps_cat');
