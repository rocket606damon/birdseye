<?php
/* Template Name: Products Listing Page */


// Get parent terms
$terms = get_terms( array(
	'taxonomy' => 'product_category',
	'parent' => 0,
	'hide_empty' => false
));

$termlist = array();
foreach($terms as $term) {
	$termlist[] = $term;
}

get_header(); ?>

<section class="hero-products prod-landing">
	<div class="container-site">
		<div class="hero-msg">
			<h1 class="headline-block lead-64" style="margin-bottom:0.65em;">
				<span class="lines lines-product blue" style="font-size:56.25%;margin-bottom:0.25em;">Our</span>
				<span>Products</span>
			</h1>
			<p class="prod-feature-title">We bring delicious and healthy together<br>with meals and sides that are easy to<br>prepare and easy to serve.</p>
		</div>
	</div>
</section>

<section class="flex-col">
	<div class="container-xl prod-grid">
		<div class="filter-toggle">
			<span data-all="Filter Products" data-less="Close Filter">Filter Products</span>
		</div>
		<div class="product-filtering content-filter">
			<form id="product-search-form" class="product-search-form">
				<input type="text" id="product-search" placeholder="Search ...">
				<input type="submit" value="submit">
			</form>

			<div id="flyout" class="product-veg">
				<p>Browse by Vegetable or Fruit</p>
				<div class="veg-filter-menu">
					<p>Select Vegetables or Fruit</p>
					<div id="filterClose" class="close">x</div>
					<div class="veg-filters">
						
						<?php 

							$terms = get_terms( 'vegetables' ); 
							for ($a = 0; $a < count($terms); $a++):

						?>

							<div>
								<input type="checkbox" value="<?php echo $terms[$a]->term_id; ?>" name="veg-filter[]" id="veg-<?php echo $a; ?>">
								<label for="veg-<?php echo $a; ?>"><?php echo $terms[$a]->name; ?></label>
							</div>


						<?php endfor; ?>

					</div>
					<div class="veg-filter-buttons">
						<button class="select-all">Select All</button>
						<button class="clear">Clear</button>
					</div>
				</div>
			</div>

			<div class="product-categories">
				<?php
					// Display parent terms
					for($a = 0; $a < count($termlist); $a++) {

						$hide = get_field('hide_category', 'term_' . $termlist[$a]->term_id);

						echo '<div class="parent" data-hide="' . $hide . '">';

							echo '<h1>' . $termlist[$a]->name . '</h1>';

							$categories = get_child_cats('product_category', $termlist[$a]->term_id);

							display_category_hierarchy('product_category', $categories, $termlist[$a]->term_id, 1); // Defined in functions.php
			
						echo '</div>';
					}
				?>
			</div>
			<span class="view-results btn btn-c show">View Results</span>
		</div>
		<div class="product-listing">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/global/ajax-loader.gif" style="width: 27px;height: 27.5px;display: block;margin: 0 auto;">
		</div>
	<!-- 	<div id="up" class="back-up">
			<span>Back to Top</span>
		</div> -->
	</div>
	
</section>

<script id="single-products-template" type="text/x-handlebars-template">
{{#each posts}}
	<div class="single-product" data-catid="{{{catID}}}">
		<a href="{{{permalink}}}">
			<div class="new" data-new="{{{new_product}}}"></div>
			<img src="" data-src="{{{image}}}" class="lazy" alt="{{{title}}}" title="{{{title}}}">
			<p class="name">{{{title}}}</p>
			<p class="cat">{{{category}}}</p>
		</a>
	</div>
{{/each}}
</script>

<?php
	get_footer();
