<?php
/** 
 *   single recipe pg
 */

get_header(); ?>

<article class="single-recipe-wrapper">

	<div class="recipe">

	<?php
		$img_field = get_field('recipe_image');
		$recipe_img = wp_get_attachment_image_src($img_field, 'full');
		echo '<img src="' . $recipe_img[0] . '" class="recipe-image original-img">';
	?>
		<a data-pin-do="buttonPin" data-pin-save="true" href="https://www.pinterest.com/pin/create/button/?url=<?php echo the_permalink(); ?>&media=<?php echo $recipe_img[0]; ?>&description=<?php the_title(); ?>"></a>

		<div class="recipe-info">
			
			<h1 class="headline-recipe"><?php the_title(); ?></h1>

			<div class="recipe-stats">
				<span><?php the_field('prep_time'); ?> min prep</span>

				<span><?php the_field('cook_time'); ?> min cook</span>

				<span><?php echo (get_field('prep_time') + get_field('cook_time')); ?> min total</span>

				<span><?php the_field('servings'); ?> servings</span>
			</div>

			<h3>Ingredients</h3>

			<?php the_field('ingredients'); ?>

			<hr>

			<h3>Directions</h3>

			<?php the_field('directions'); ?>

		</div>
	</div>

	<div class="related-products">

		<?php 
		/*

			// Related products from legacy IDs
			$legacy_ids = get_field('related_product_legacy_ids'); 

			if ($legacy_ids != '') {
				
				$legacy_ids = explode(',', $legacy_ids);
				
				$ids = array(
					'relation' => 'OR'
				);

				for ($a = 0; $a < count($legacy_ids); $a++) {
					$ids[] = array(
						'key'		=> 'legacy_id',
						'compare'	=> '=',
						'value'		=> $legacy_ids[$a],
					);
				}

				// args
				$args = array(
					'numberposts'	=> -1,
					'post_type'		=> 'product',
					'meta_query'	=> $ids
				);
				
				$the_query = new WP_Query( $args );

		?>
				<?php if( $the_query->have_posts() ): ?>
					<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
						
						<section class="panel panel-products">
							<div class="container-site flex-row">

								<div class="product-image">
									<img src="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' )[0]; ?>">
								</div>
								<div class="product-info">
									<h1 class="headline-l"><?php the_title(); ?></h1>
									
									<div class="the-content"><?php the_content(); ?></div>
									<hr>
									<div class="available-in">Available in: <span><?php the_field('available_in'); ?></span></div>
									<hr>
									<div class="nutrition-facts">Nutrition Facts</div>
									<hr>

									<?php 
										$related_recipes = get_field('recipe_panel');
										if($related_recipes) {
											$id = '#relatedRecipes';
										} 
									?>
									<div class="btns flex-row">
										<a href="" class="btn btn-s">Where to buy</a>
										<?php if($related_recipes) : ?>
											<a href="#relatedRecipes" class="btn btn-m">Recipe suggestions</a>	
										<?php endif; ?>
									</div>
								</div>

							</div>
						</section>

					<?php endwhile; ?>
				<?php endif; ?>

				<?php wp_reset_query();	 // Restore global post data stomped by the_post(). ?>
				<?php
			}
		*/
		?>

		<?php
			// Related products from relationship field
			$posts = get_field('related_products');
			if( $posts ): ?>

				<div class="container-site flex-row">
					<h2 class="headline-f">Product Used in This Recipe</h2>
				</div>

			    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
			        <?php setup_postdata($post); ?>
			        	<?php include('components/panels/product-info.php'); ?>
			    <?php endforeach; ?>
			    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
			<?php endif; ?>

	</div>

	<div class="related-recipes">
		<h2 class="headline-s">Related Recipes</h2>
		<p class="support-copy">Easy to prepare. Easy to serve.</p>

		<div class="container-site recipe-container flex-row">
		<?php

			$max_related = 3;
			$exclude = array();

			// Get related recipes based on custom field
			$posts = get_field('related_recipes');

			if( $posts ):
			
				foreach( $posts as $post): // variable must be called $post (IMPORTANT)

					setup_postdata($post);
			
					include('components/panels/related-recipe-single.php');				
					$exclude[] = $post->ID;
					$max_related -= 1;
			
				endforeach;
			
				wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly

			endif;


			// Get related recipes based on custom tax
			$tax_query = array();
			$tax_query['relation'] = 'OR';
			$terms = get_the_terms($post->ID, 'recipe_categories');
			$exclude[] = $post->ID;

			for($a = 0; $a < count($terms); $a++) {

				$tax_query[] =  array(
					'taxonomy' => 'recipe_categories',
					'field' => 'term_id',
					'terms' => $terms[$a]
				);

			}

			$query = new WP_Query( 
				array(
					'post_type' => 'recipe',
					'posts_per_page' => $max_related,
					'post__not_in' => $exclude,
					'orderby' => 'rand',
					'tax_query' => $tax_query
				)
			);

			if ($query->have_posts()) {

				while ($query->have_posts()) {

					$query->the_post();

					include('components/panels/related-recipe-single.php');
					$exclude[] = $post->ID;
					$max_related -= 1;

				}

			}

			if ($max_related > 0) {
				// Get random recipes
				$query = new WP_Query( 
					array(
						'post_type' => 'recipe',
						'posts_per_page' => $max_related,
						'post__not_in' => $exclude,
						'orderby' => 'rand'
					)
				);

				if ($query->have_posts()) {

					while ($query->have_posts()) {

						$query->the_post();

						include('components/panels/related-recipe-single.php');

					}

				}
			}

		?>
		</div>

	</div>
	
</section>


<?php
get_footer();
