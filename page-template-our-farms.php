<?php
/* Template Name: Our Farms */

//kd_enqueue_stylesheet('farms');
get_header(); ?>

<section class="hero-farms">
	<div class="hero-msg">
		<h1 class="headline-block white lead-64" style="margin-bottom:0.5em;">
			<span>Fresh</span>
			<span class="cap lines lines-farm" style="font-size:28.13%;margin:0.25em 0 -0.5em;">From</span>
			<span class="cap" style="font-size:56.25%;">The Farm</span>
		</h1>
		<p class="prod-feature-title">The only thing that matters to us <br>is quality vegetables. Our farmers <br>pick only the best vegetables, and <br>we freeze them in a flash to <br>preserve their freshness.</p>
	</div>
</section>

<section class="flex-row img-blocks">
	<div class="img-block flex-col source-map-block overlay-btm">
		<div class="block-content">
			<h2 class="headline-s white"><?php the_field('title_farms_left'); ?></h2>
			<p><?php the_field('copy_farms_left'); ?></p>
			<?php if(get_field('cta_button_link_farms_pg')) : ?>
				<a href="<?php the_field('cta_button_link_farms_pg'); ?>" class="btn btn-l"><?php the_field('cta_button_copy_farms_pg'); ?></a>	
			<?php endif; ?>
		</div>
	</div>
	<div class="img-block flex-col field-table-block">
		<div class="block-content">
			<h2 class="headline-s white"><?php the_field('title_farms_right'); ?></h2>
			<p><?php the_field('copy_farms_right'); ?></p>	
		</div>
	</div>
</section>

<?php 
	include('components/panels/media.php'); 
?>

<section class="page-links flex-row container-site">
	<a href="<?php echo home_url(); ?>/frozen-vs-fresh" class="page-link-fresh flex-col">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/images/global/pg-link-fresh.jpg">
		<h3 class="prod-feature-title">Fresh Factors</h3>	
	</a>
	<a href="<?php echo home_url(); ?>/our-story/our-mission" class="page-link-mission flex-col">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/images/global/pg-link-mission.jpg">
		<h3 class="prod-feature-title">Our Mission</h3>	
	</a>
	<a href="<?php echo home_url(); ?>/our-story/our-roots" class="page-link-roots flex-col">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/images/global/pg-link-roots.jpg">
		<h3 class="prod-feature-title">Our Roots</h3>	
	</a>
</section>

<?php
	get_footer(); 