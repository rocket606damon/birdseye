<?php
/* Template Name: New From Birds Eye */

//kd_enqueue_stylesheet('new');
get_header(); ?>

<section class="hero-slider media-slider">

	<?php if(have_rows('new_slider')) : ?>
		<ul class="slider-hero">
			<?php while(have_rows('new_slider')) : the_row(); ?>
				<?php
					$slide_alt = get_sub_field('new_slide_title');
					$img = get_sub_field('new_slide_image');
					$img_s = wp_get_attachment_image_src($img, 'slider-s');
					$img_m = wp_get_attachment_image_src($img, 'slider-m');
					$img_l = wp_get_attachment_image_src($img, 'slider-l');
				?>
					<li class="new-from-slide" title="<?php echo $slide_alt; ?>">
						<picture>
							<!--[if IE 9]><video style="display: none;"><![endif]-->
							<source media="(min-width: 87.5em)" srcset="<?php echo $img_l[0]; ?>" />
							<source media="(min-width: 50em)" srcset="<?php echo $img_m[0]; ?>" />
							<!--[if IE 9]></video><![endif]-->
							<img src="<?php echo $img_s[0]; ?>" alt="<?php echo $slide_alt; ?>" />
						</picture>
					</li>
				<?php endwhile; ?>
		</ul>
	<?php endif; ?>

</section>

<?php
	include('components/product-lineup.php');

	get_footer(); 