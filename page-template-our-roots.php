<?php
/* Template Name: Our Roots */

//kd_enqueue_stylesheet('roots');
get_header(); ?>

<section class="hero-roots">
	<div class="hero-msg">
		<h1 class="headline-block white lead-64">
			<span class="cap" style="font-size:56.25%;">Great Vegetables</span>
			<span class="cap lines lines-roots" style="font-size:28.125%;">Spring from Great</span>
			<span>Roots</span>
		</h1>
	</div>
</section>

<section class="panel panel-clarence panel-rootspg" title="Clarence Birdseye">
	<div class="container-site flex-col">
		<div class="panel-msg">
			<h2 class="headline-s"><?php the_field('story_block_a_title'); ?></h2>
			<p><?php the_field('story_block_a_copy'); ?></p>
		</div>	
	</div>
</section>

<section class="panel flex-row panel-quick-freeze">
	<div id="snow" class="flex-col bg-gradient">
		<h2 class="headline-s white"><?php the_field('story_block_b_title'); ?></h2>
	</div>
	<div class="flex-col"> 
		<img src="<?php echo get_template_directory_uri(); ?>/assets/images/our-roots/quick-freeze-diagram.gif">
	</div>
</section>
<section class="panel panel-veg-quad container-site">
	<picture>
		<!--[if IE 9]><video style="display: none;"><![endif]-->
		<source media="(min-width: 50em)" srcset="<?php echo get_template_directory_uri(); ?>/assets/images/our-roots/veg-quad-bowl@2x.jpg" />
		<!--[if IE 9]></video><![endif]-->
		<img src="<?php echo get_template_directory_uri(); ?>/assets/images/our-roots/veg-quad-bowl.jpg" alt="" />
	</picture>
	<p><?php the_field('story_block_c_left'); ?></p>
	<p><?php the_field('story_block_c_right'); ?></p>
</section>

<section class="panel panel-boxtruck panel-rootspg">
	<div class="container-site flex-col">
		<div class="panel-msg">
			<h2 class="headline-s"><?php the_field('story_block_d_title'); ?></h2>
			<p><?php the_field('story_block_d_copy'); ?></p>
		</div>	
	</div>
</section>

<?php include('components/panels/freezer-aisle.php'); ?>

<section class="page-links pad-top flex-row container-site">
	<a href="<?php echo home_url(); ?>/frozen-vs-fresh" class="page-link-fresh flex-col">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/images/global/pg-link-fresh.jpg">
		<h3 class="prod-feature-title">Fresh Factors</h3>	
	</a>
	<a href="<?php echo home_url(); ?>/our-story/our-mission" class="page-link-mission flex-col">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/images/global/pg-link-mission.jpg">
		<h3 class="prod-feature-title">Our Mission</h3>	
	</a>
	<a href="<?php echo home_url(); ?>/our-story/our-farms" class="page-link-roots flex-col">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/images/global/pg-link-farms.jpg">
		<h3 class="prod-feature-title">Our Farms</h3>	
	</a>
</section>

<?php
	get_footer(); 