<?php
/* Template Name: Contact Thank You pg */

//kd_enqueue_stylesheet('frozenfresh');
get_header(); ?>

<section class="panel contact-intro container-s" style="text-align: center;">
	<h1 class="headline-s"><?php the_title(); ?></h1>
	<?php 
		while(have_posts()) : the_post();
			the_content();
		endwhile;
	?>
</section>

<?php
	get_footer(); 